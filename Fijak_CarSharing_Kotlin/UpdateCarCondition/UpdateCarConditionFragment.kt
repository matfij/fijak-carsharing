package com.corcode.asystententerprise.UI.UpdateCarCondition

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.corcode.asystententerprise.App
import com.corcode.asystententerprise.R
import com.corcode.asystententerprise.Bus.ProgressVisibility
import com.corcode.asystententerprise.Bus.RxBus
import com.corcode.asystententerprise.Bus.ShowMessage
import com.corcode.asystententerprise.UI.ManageOrder.ManageOrderFragment
import com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView
import com.corcode.asystententerprise.Utils.*
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.fragment_handcar.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class UpdateCarConditionFragment: Fragment(), UpdateCarConditionMvpView {

    // view controller for performing data operations
    @Inject
    lateinit var presenter: UpdateCarConditionPresenter

    // view creation - xml layout preparation, dependency injection, wiring up view (fragment) and controller (presenter),
    // set top bar layout (title)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_handcar, container, false)
        App.component.inject(this)
        presenter.attachView(this)
        (activity as NavigationMvpView).setToolbarTitle(R.string.update_car_condition)
        return view
    }

    // fragment lifecycle invoked after view creation
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.start()
        setupClickListeners()
        setupFrontTiresTextChangeListener()
        setupBackTiresTextChangeListener()
        setupSameAxlesChangeListener()
        observeWheelsType()
    }

    // wiring up presenter logic to fragment buttons
    private fun setupClickListeners() {
        handcar_button_next.setOnClickListener {
            if(validateRequiredFields()) {
                presenter.onNextClick()
            }
        }
    }

    // validate required form firlds
    private fun validateRequiredFields(): Boolean {
        return if(handcar_course_input.text.isNullOrBlank() 
        || handcar_fuel_input.text.isNullOrBlank() 
        || handcar_front_tires_input.text.isNullOrBlank() 
        ||handcar_back_tires_input.text.isNullOrBlank()) {
            context!!.toast(getString(R.string.required_fields_error))
            false
        } else {
            true
        }
    }

    // enable tires editing (may be implemented in xml view)
    override fun enableTiresEditing() {
        handcar_front_tires_summer.isEnabled = true
        handcar_front_tires_winter.isEnabled = true
        handcar_front_tires_universal.isEnabled = true
        handcar_front_wheels_aluminum.isEnabled = true
        handcar_front_wheels_steel.isEnabled = true
        handcar_front_manufacturer_input.isEnabled = true
        handcar_front_tires_input.isEnabled = true

        handcar_back_tires_summer.isEnabled = true
        handcar_back_tires_winter.isEnabled = true
        handcar_back_tires_universal.isEnabled = true
        handcar_back_wheels_aluminum.isEnabled = true
        handcar_back_wheels_steel.isEnabled = true
        handcar_back_manufacturer_input.isEnabled = true
        handcar_back_tires_input.isEnabled = true

        sameAxlesCheckbox.isEnabled = true
    }

    // observe upholsery purity status and pass changes to presenter
    override fun observeUpholsteryPurity() {
        handcar_upholstery_dirty.setOnClickListener {
            presenter.markUpholsteryPurity(true)
        }
        handcar_upholstery_clean.setOnClickListener {
            presenter.markUpholsteryPurity(false)
        }
    }

    // display fetched car registration number, don't allow to edit it
    override fun setRegistrationNumberInput(registrationNumber: String?) {
        registrationNumber?.let {
            handcar_registration_number_input.text = SpannableStringBuilder(registrationNumber)
        }
    }
    override fun disableRegistrationNumberInput() {
        handcar_registration_number_input.isEnabled = false
    }

    // enable independent tires edit for front and back tires if they are not the same
    private fun setupSameAxlesChangeListener() {
        sameAxlesCheckbox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                hideBackAxle(false)
            } else {
                showBackAxle()
            }
        }
    }

    /*
    * handle tires size input text completion in xxx/xx/xxx format
    */
    private fun setupFrontTiresTextChangeListener() {
        var length = 0
        handcar_front_tires_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                length = handcar_front_tires_input.text.toString().length
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val text = handcar_front_tires_input.text.toString()
                if (text.length == 3 && length < text.length ||
                        text.length == 6 && length < text.length) {
                    handcar_front_tires_input.text = SpannableStringBuilder("$s/")
                    handcar_front_tires_input.setSelection(handcar_front_tires_input.text.length)
                } else if (text.length == 7 && length < text.length) {
                    handcar_front_tires_input.text = SpannableStringBuilder("${s}R")
                    handcar_front_tires_input.setSelection(handcar_front_tires_input.text.length)
                } else if (text.length > 10) {
                    handcar_front_tires_input.text = SpannableStringBuilder(text.substring(0, 10))
                    handcar_front_tires_input.setSelection(handcar_front_tires_input.text.length)
                }
            }
        })
    }
    private fun setupBackTiresTextChangeListener() {
        var length = 0
        handcar_back_tires_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                length = handcar_back_tires_input.text.toString().length
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val text = handcar_back_tires_input.text.toString()
                if (text.length == 3 && length < text.length ||
                        text.length == 6 && length < text.length) {
                    handcar_back_tires_input.text = SpannableStringBuilder("$s/")
                    handcar_back_tires_input.setSelection(handcar_back_tires_input.text.length)
                } else if (text.length == 7 && length < text.length) {
                    handcar_back_tires_input.text = SpannableStringBuilder("${s}R")
                    handcar_back_tires_input.setSelection(handcar_back_tires_input.text.length)
                } else if (text.length > 10) {
                    handcar_back_tires_input.text = SpannableStringBuilder(text.substring(0, 10))
                    handcar_back_tires_input.setSelection(handcar_back_tires_input.text.length)
                }
            }
        })
    }

    /*
    * hide or show back tires edit based on sameAxlesCheckbox value
     */
    override fun hideBackAxle(checked: Boolean) {
        if(checked) sameAxlesCheckbox.isChecked = true
        handcar_back_tires_text.gone()
        handcar_back_tires_group.gone()
        handcar_back_manufacturer.gone()
        handcar_back_tires_layout.gone()
        handcar_back_wheels_type.gone()
        handcar_front_tires_text.setText(R.string.hand_car_tires)
        handcar_back_manufacturer_input.text = handcar_front_manufacturer_input.text
        handcar_back_tires_input.text = handcar_front_tires_input.text
        handcar_back_tires_summer.isChecked = handcar_front_tires_summer.isChecked
        handcar_back_tires_winter.isChecked = handcar_front_tires_winter.isChecked
        handcar_back_tires_universal.isChecked = handcar_front_tires_universal.isChecked
        handcar_back_wheels_aluminum.isChecked = handcar_front_wheels_aluminum.isChecked
        handcar_back_wheels_steel.isChecked = handcar_front_wheels_steel.isChecked
    }
    override fun showBackAxle() {
        handcar_back_tires_text.visible()
        handcar_back_tires_group.visible()
        handcar_back_manufacturer.visible()
        handcar_back_tires_layout.visible()
        handcar_back_wheels_type.visible()
        handcar_front_tires_text.setText(R.string.hand_car_front_tires)
    }

    // observe wheels type radio buttons and control caps input behaviour based on wheels type
    private fun observeWheelsType() {
        handcar_front_wheels_aluminum.setOnClickListener {
            if(handcar_front_wheels_aluminum.isChecked && handcar_back_wheels_aluminum.isChecked || (handcar_front_wheels_aluminum.isChecked && sameAxlesCheckbox.isChecked)) {
                handcar_caps_input.setText("0")
                handcar_caps_input.isEnabled = false
            }
        }
        handcar_front_wheels_steel.setOnClickListener {
            handcar_caps_input.isEnabled = true
        }
        handcar_back_wheels_aluminum.setOnClickListener {
            if(handcar_front_wheels_aluminum.isChecked && handcar_back_wheels_aluminum.isChecked) {
                handcar_caps_input.setText("0")
                handcar_caps_input.isEnabled = false
            }
        }
        handcar_back_wheels_steel.setOnClickListener {
            handcar_caps_input.isEnabled = true
        }
    }

    /*
    * prepopulate car condition form with fetched data (passed by presenter)
     */
    override fun setupFirstPart(registrationNumber: String?, brandName: String?, modelName: String?, mileage: Int?, fuelState: Int?) {
        registrationNumber?.let {
            handcar_registration_no.text = registrationNumber
        }
        brandName?.let { handcar_model.text = "$brandName " }
        modelName?.let {
            val brand = handcar_model.text
            handcar_model.text = "$brand $modelName"
        }
        mileage?.let { handcar_course_input.text = SpannableStringBuilder("$mileage") }
        fuelState?.let { handcar_fuel_input.text = SpannableStringBuilder(fuelState.toInt().toString()) }
    }
    override fun setupSecondPart(cleanInside: Boolean, cleanOutside: Boolean, cleanUpholstery: Boolean, frontTiresType: Int?, frontTiresProducer: String?, frontTiresSize: String?, backTiresType: Int?, backTiresProducer: String?, backTiresSize: String?, frontWheelsType: Int?, backWheelsType: Int?) {
        if (cleanInside) handcar_interior_clean.isChecked = true
        else handcar_interior_dirty.isChecked = true
        if (cleanOutside) handcar_exterior_clean.isChecked = true
        else handcar_exterior_dirty.isChecked = true
        if (cleanUpholstery) handcar_upholstery_clean.isChecked = true
        else handcar_upholstery_dirty.isChecked = true

        if(presenter.shouldClearTires()) {
            showBackAxle()
            sameAxlesCheckbox.isChecked = false
            handcar_front_tires_universal.isChecked = true
            handcar_back_tires_universal.isChecked = true
            handcar_front_wheels_steel.isChecked = true
            handcar_back_wheels_steel.isChecked = true
        } else {
            when (frontTiresType) {
                CarTires.TIRES_SUMMER -> handcar_front_tires_summer.isChecked = true
                CarTires.TIRES_WINTER -> handcar_front_tires_winter.isChecked = true
                CarTires.TIRES_UNIVERSAL -> handcar_front_tires_universal.isChecked = true
            }
            frontTiresProducer?.let { handcar_front_manufacturer_input.text = SpannableStringBuilder(frontTiresProducer) }
            frontTiresSize?.let { handcar_front_tires_input.text = SpannableStringBuilder(formatTiresSize(frontTiresSize)) }
            if (frontWheelsType == CarTires.TIRES_ALUMINUM) handcar_front_wheels_aluminum.isChecked = true
            else handcar_front_wheels_steel.isChecked = true

            when (backTiresType) {
                CarTires.TIRES_SUMMER -> handcar_back_tires_summer.isChecked = true
                CarTires.TIRES_WINTER -> handcar_back_tires_winter.isChecked = true
                CarTires.TIRES_UNIVERSAL -> handcar_back_tires_universal.isChecked = true
            }
            backTiresProducer?.let { handcar_back_manufacturer_input.text = SpannableStringBuilder(backTiresProducer) }
            backTiresSize?.let { handcar_back_tires_input.text = SpannableStringBuilder(formatTiresSize(backTiresSize)) }
            if (backWheelsType == CarTires.TIRES_ALUMINUM) handcar_back_wheels_aluminum.isChecked = true
            else handcar_back_wheels_steel.isChecked = true
        }
    }

    // diplay tire size in xxx/xx/xxx format
    private fun formatTiresSize(size: String?): String? {
        return if(size?.length != 10) {
            try {
                val first = size?.substring(IntRange(0,2))
                val second = size?.substring(IntRange(3,4))
                val third = "R${size?.substring(IntRange(5,6))}"
                listOf(first, second, third).joinToString("/")
            } catch(t: Throwable) { size }
        } else {
            size
        }
    }

    // prepopulate checkboxes values with fetched data
    override fun setupThirdPart(obligatoryEq: List<Int>, optionalEq: List<Int>, caps: Int?) {
        obligatoryEq.forEach { i ->
            when (i) {
                CarObligatoryEquipment.FIRE_EXTINGUISHER -> handcar_extinguisher.isChecked = true
                CarObligatoryEquipment.FIRST_AID_KIT -> handcar_first_aid_kit.isChecked = true
                CarObligatoryEquipment.TRIANGLE -> handcar_triangle.isChecked = true
                CarObligatoryEquipment.REGISTRATION_DOCUMENT -> handcar_registration_document.isChecked = true
                CarObligatoryEquipment.OC_INSURANCE -> handcar_oc_insurance.isChecked = true
                CarObligatoryEquipment.KEYS -> handcar_keys.isChecked = true
            }
        }
        optionalEq.forEach { i ->
            when (i) {
                CarOptionalEquipment.AERIAL -> handcar_antenna.isChecked = true
                CarOptionalEquipment.BOOT_COVER -> handcar_blind.isChecked = true
                CarOptionalEquipment.CAR_LIFTING_TOOLS -> handcar_jack.isChecked = true
                CarOptionalEquipment.GPS -> handcar_gps.isChecked = true
                CarOptionalEquipment.SPARE_WHEEL -> handcar_spare_wheel.isChecked = true
                CarOptionalEquipment.TIRE_REPAIR_KIT -> handcar_repair_kit.isChecked = true
                CarOptionalEquipment.ROOF_RAILING -> handcar_roof_railing.isChecked = true
                CarOptionalEquipment.SECOND_KEY_SET -> handcar_second_key_set.isChecked = true
                CarOptionalEquipment.USER_MANUAL -> handcar_user_manual.isChecked = true
                CarOptionalEquipment.FLOOR_MATS -> handcar_floor_mats.isChecked = true
            }
        }
        if(presenter.shouldClearTires()) {
            handcar_caps_input.setText("0")
        } else {
            caps?.let { handcar_caps_input.text = SpannableStringBuilder("$caps") }
        }
    }

    // validate tires input form fields
    override fun isFormValid(): Boolean {
        return if (Pattern.TIRE_SIZE.matches(handcar_front_tires_input.text.toString()) &&
                Pattern.TIRE_SIZE.matches(handcar_back_tires_input.text.toString())) {
            true
        } else {
            RxBus.post(ShowMessage(R.string.hand_car_tires_error))
            false
        }
    }

    /**
     * alert dialogs shown to the user when supposingly incorrect mileage input is entered
     */
    override fun showMileageLowerWarning() {
        val builder = AlertDialog.Builder(context!!)
        val title = getString(R.string.hand_car_lower_mileage_warning_title)
        builder.setTitle(title)
        val message = getString(R.string.hand_car_mileage_warning)
        builder.setMessage(message)
        builder.setNeutralButton(R.string.my_tasks_understood) {
            _, _ ->
        }
        builder.show()
    }
    override fun showMileageNotChangedWarning() {
        val builder = AlertDialog.Builder(context!!)
        val title = getString(R.string.hand_car_equal_mileage_warning_title)
        builder.setTitle(title)
        val message = getString(R.string.hand_car_mileage_warning)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.my_tasks_understood) {
            _, _ ->
            presenter.doNext()
        }
        builder.setNegativeButton(R.string.cancel) {
            _, _->
        }
        builder.show()
    }
    override fun showMileageTooHighWarning() {
        val builder = AlertDialog.Builder(context!!)
        val title = getString(R.string.hand_car_higher_mileage_warning_title)
        builder.setTitle(title)
        val message = getString(R.string.hand_car_mileage_warning)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.my_tasks_understood) {
            _, _ ->
            presenter.doNext()
        }
        builder.setNegativeButton(R.string.cancel) {
            _, _->
        }
        builder.show()
    }

    /**
     * methods for getting input field values
     */
    override fun getCourse(): Int = handcar_course_input.text.toString().trim().toInt()

    override fun getFuel(): Int = handcar_fuel_input.text.toString().trim().toInt()

    override fun getCleanInside(): Boolean = handcar_interior_clean.isChecked

    override fun getCleanOutside(): Boolean = handcar_exterior_clean.isChecked

    override fun getCleanUpholstery(): Boolean = handcar_upholstery_clean.isChecked

    override fun getFrontTiresSummer(): Boolean = handcar_front_tires_summer.isChecked

    override fun getFrontTiresWinter(): Boolean = handcar_front_tires_winter.isChecked

    override fun getFrontTiresUniversal(): Boolean = handcar_front_tires_universal.isChecked

    override fun getBackTiresSummer(): Boolean = handcar_back_tires_summer.isChecked

    override fun getBackTiresWinter(): Boolean = handcar_back_tires_winter.isChecked

    override fun getBackTiresUniversal(): Boolean = handcar_back_tires_universal.isChecked

    override fun getFrontTiresManufacturer(): String = handcar_front_manufacturer_input.text.toString().trim()

    override fun getBackTiresManufacturer(): String = handcar_back_manufacturer_input.text.toString().trim()

    override fun getFrontTiresSize(): String = handcar_front_tires_input.text.toString().trim()

    override fun getFrontWheelsType(): Boolean = handcar_front_wheels_aluminum.isChecked

    override fun getBackWheelsType(): Boolean = handcar_back_wheels_aluminum.isChecked

    override fun getBackTiresSize(): String = handcar_back_tires_input.text.toString().trim()

    override fun checkSameAxles(): Boolean = sameAxlesCheckbox.isChecked

    override fun getAntenna(): Boolean = handcar_antenna.isChecked

    override fun getSecondKeySet(): Boolean? = handcar_second_key_set?.isChecked

    override fun getUserManual(): Boolean? = handcar_user_manual?.isChecked

    override fun getFloorMats(): Boolean? = handcar_floor_mats?.isChecked

    override fun getCaps(): Int = handcar_caps_input.text.toString().toInt()

    override fun getBlind(): Boolean = handcar_blind.isChecked

    override fun getSpareWheel(): Boolean = handcar_spare_wheel.isChecked

    override fun getJack(): Boolean = handcar_jack.isChecked

    override fun getRepairKit(): Boolean = handcar_repair_kit.isChecked

    override fun getGPS(): Boolean = handcar_gps.isChecked

    override fun getRoofRailing(): Boolean = handcar_roof_railing.isChecked

    override fun getFireExtinguisher(): Boolean = handcar_extinguisher.isChecked

    override fun getFirstAidKit(): Boolean = handcar_first_aid_kit.isChecked

    override fun getTriangle(): Boolean = handcar_triangle.isChecked

    override fun getRegistrationDocument(): Boolean = handcar_registration_document.isChecked

    override fun getOCInsurance(): Boolean = handcar_oc_insurance.isChecked

    override fun getKeys(): Boolean = handcar_keys.isChecked


    // progress indicator shown during data transfers
    override fun showProgress() {
        handcar_layout.invisible()
        RxBus.post(ProgressVisibility(true))
    }
    override fun hideProgress() {
        handcar_layout.visible()
        RxBus.post(ProgressVisibility(false))
    }

}
