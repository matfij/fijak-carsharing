package com.corcode.asystententerprise.UI.UpdateCarCondition

import com.corcode.asystententerprise.R
import com.corcode.asystententerprise.Base.BasePresenter
import com.corcode.asystententerprise.Bus.*
import com.corcode.asystententerprise.Persistance.Repository
import com.corcode.asystententerprise.Schemas.*
import com.corcode.asystententerprise.Utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UpdateCarConditionPresenter @Inject constructor(private val repository: Repository): BasePresenter<UpdateCarConditionMvpView>() {

    // car model object for storing car data
    private var car: CarData? = null

    // container that gets rid of resources used inside him, after finishing their tasks
    private val composite = CompositeDisposable()

    fun start() {
        // get car data from global storing object (fetched during fragments change)
        car = repository.carManager.carData

        getView()?.enableTiresEditing()
    }

    // hide back tires edit fields if they are the same as front tires
    private fun checkAxles(car: CurrentCondition) {
        if(car.frontTires?.producer == car.backTires?.producer 
            && car.frontTires?.size == car.backTires?.size 
            && car.frontTires?.type == car.backTires?.type 
            && car.frontTires?.wheelsType == car.backTires?.wheelsType) {

            getView()?.hideBackAxle(true)
        }
    }

    // prepopulate edit car form
    private fun setCarData(refreshRegistration: Boolean = true) {
        val carData = repository.carManager.carData
        carData?.carCondition?.let {

            // Check axles
            checkAxles(it)

            // Get lists of equipment types
            val obligatoryEq = BitUtils.getCarObligatoryEquipment(it.obligatoryEquipment)
            val optionalEq = BitUtils.getCarOptionalEquipment(it.optionalEquipment)

            // save initial car data for later validation
            repository.carManager.initialCarCondition = it.copy()

            // Get car purity based on enum values
            val cleanInside: Boolean
            val cleanOutside: Boolean
            val cleanUpholstery: Boolean

            when (it.purityState) {
                CarPurity.CLEAN_INSIDE -> {
                    cleanInside = true
                    cleanOutside = false
                    cleanUpholstery = false
                }
                CarPurity.CLEAN_OUTSIDE -> {
                    cleanInside = false
                    cleanOutside = true
                    cleanUpholstery = true
                }
                CarPurity.CLEAN_UPHOLSTERY -> {
                    cleanInside = false
                    cleanOutside = false
                    cleanUpholstery = true
                }
                CarPurity.CLEAN_OUTSIDE + CarPurity.CLEAN_INSIDE -> {
                    cleanInside = true
                    cleanOutside = true
                    cleanUpholstery = false
                }
                CarPurity.CLEAN_INSIDE + CarPurity.CLEAN_UPHOLSTERY -> {
                    cleanInside = true
                    cleanOutside = false
                    cleanUpholstery = true
                }
                CarPurity.CLEAN_OUTSIDE + CarPurity.CLEAN_UPHOLSTERY -> {
                    cleanInside = false
                    cleanOutside = true
                    cleanUpholstery = true
                }
                CarPurity.CLEAN_INSIDE + CarPurity.CLEAN_OUTSIDE + CarPurity.CLEAN_UPHOLSTERY -> {
                    cleanInside = true
                    cleanOutside = true
                    cleanUpholstery = true
                }
                else -> {
                    cleanInside = false
                    cleanOutside = false
                    cleanUpholstery = false
                }
            }

            // prepopulate form inputs
            getView()?.setRegistrationNumberInput(carData.registrationNo)
            getView()?.setupFirstPart(
                carData.registrationNo,
                carData.carBrandName,
                carData.carModelName,
                it.mileage,
                (it.fuelState)
            )
            getView()?.setupSecondPart(
                cleanInside,
                cleanOutside,
                cleanUpholstery,
                it.frontTires?.type,
                it.frontTires?.producer,
                it.frontTires?.size,
                it.backTires?.type,
                it.backTires?.producer,
                it.backTires?.size,
                it.frontTires?.wheelsType,
                it.backTires?.wheelsType
            )
            getView()?.setupThirdPart(
                obligatoryEq,
                optionalEq,
                it.wheelCupsAmount
            )

            getView()?.observeUpholsteryPurity()
        }
    }

    /*
    * handle navigation
    */
    fun onNextClick() {

        // validate updated car properties
        val condition = repository.carManager.initialCarCondition
        if (condition != null) {
            val course = getView()?.getCourse()
            if (course != null) {
                when {
                    course < condition.mileage -> {
                        getView()?.showMileageLowerWarning()
                    }
                    course == condition.mileage -> {
                        getView()?.showMileageNotChangedWarning()
                    }
                    course >= (condition.mileage + 1000) -> {
                        getView()?.showMileageTooHighWarning()
                    }
                    else -> {
                        doNext()
                    }
                }
            }
        }
    }
    fun doNext() {
        getInputsAndUpdateCarCondition()
        // takie photo of dirty car part
        if(repository.carManager.dirtyUpholstery && !repository.carManager.isDirtyPhotoTaken!!) {
            getView()?.hideProgress()
            RxBus.post(ChangeFragment(FragmentTag.ADD_PHOTO))
            repository.carManager.isDirtyPhotoTaken = false
        } 
        // proceed to next fragment
        else {
            updateCarCondition()
        }
    }

    // collect car data from the form
    private fun getInputsAndUpdateCarCondition() {
        val initialCar = repository.carManager.carData
        if (initialCar != null) {
            initialCar.carCondition?.let {
                with(it) {
                    getView()?.let {
                        var optionalEquipmentSum = 0
                        var obligatoryEquipmentSum = 0
                        var purityStateSum = 0

                        // Course
                        mileage = it.getCourse()

                        // Fuel
                        fuelState = it.getFuel()

                        // Purity state
                        if (it.getCleanInside()) purityStateSum += CarPurity.CLEAN_INSIDE
                        if (it.getCleanOutside()) purityStateSum += CarPurity.CLEAN_OUTSIDE
                        if (it.getCleanUpholstery()) purityStateSum += CarPurity.CLEAN_UPHOLSTERY
                        purityState = purityStateSum

                        // Tires
                        if(frontTires == null) {
                            val tiresType = when {
                                it.getFrontTiresSummer() -> CarTires.TIRES_SUMMER
                                it.getFrontTiresWinter() -> CarTires.TIRES_WINTER
                                else -> CarTires.TIRES_UNIVERSAL
                            }
                            val wheelsType = if (it.getFrontWheelsType()) CarTires.TIRES_ALUMINUM else CarTires.TIRES_STEEL
                            val tiresProducer = it.getFrontTiresManufacturer()
                            val tiresSize = it.getFrontTiresSize()
                            frontTires = Tires().createTiresObject(tiresType, tiresProducer, tiresSize, wheelsType)
                        } else {
                            frontTires?.type = when {
                                it.getFrontTiresSummer() -> CarTires.TIRES_SUMMER
                                it.getFrontTiresWinter() -> CarTires.TIRES_WINTER
                                else -> CarTires.TIRES_UNIVERSAL
                            }
                            frontTires?.wheelsType = if (it.getFrontWheelsType()) CarTires.TIRES_ALUMINUM else CarTires.TIRES_STEEL
                            frontTires?.producer = it.getFrontTiresManufacturer()
                            frontTires?.size = it.getFrontTiresSize()
                        }
                        if(backTires == null) {
                            val tiresType = when {
                                it.getBackTiresSummer() -> CarTires.TIRES_SUMMER
                                it.getBackTiresWinter() -> CarTires.TIRES_WINTER
                                else -> CarTires.TIRES_UNIVERSAL
                            }
                            val wheelsType = if (it.getBackWheelsType()) CarTires.TIRES_ALUMINUM else CarTires.TIRES_STEEL
                            val tiresProducer = it.getBackTiresManufacturer()
                            val tiresSize = it.getBackTiresSize()
                            backTires = Tires().createTiresObject(tiresType, tiresProducer, tiresSize, wheelsType)
                        } else {
                            backTires?.type = when {
                                it.getBackTiresSummer() -> CarTires.TIRES_SUMMER
                                it.getBackTiresWinter() -> CarTires.TIRES_WINTER
                                else -> CarTires.TIRES_UNIVERSAL
                            }
                            backTires?.wheelsType = if (it.getBackWheelsType()) CarTires.TIRES_ALUMINUM else CarTires.TIRES_STEEL
                            backTires?.producer = it.getBackTiresManufacturer()
                            backTires?.size = it.getBackTiresSize()
                        }
                        if(getView()?.checkSameAxles() == true) {
                            backTires?.type = frontTires!!.type
                            backTires?.wheelsType = frontTires!!.wheelsType
                            backTires?.producer = frontTires!!.producer
                            backTires?.size = frontTires!!.size
                        }

                        // Optional equipment
                        if (it.getAntenna()) optionalEquipmentSum += CarOptionalEquipment.AERIAL
                        if (it.getBlind()) optionalEquipmentSum += CarOptionalEquipment.BOOT_COVER
                        if (it.getSpareWheel()) optionalEquipmentSum += CarOptionalEquipment.SPARE_WHEEL
                        if (it.getJack()) optionalEquipmentSum += CarOptionalEquipment.CAR_LIFTING_TOOLS
                        if (it.getRepairKit()) optionalEquipmentSum += CarOptionalEquipment.TIRE_REPAIR_KIT
                        if (it.getGPS()) optionalEquipmentSum += CarOptionalEquipment.GPS
                        if (it.getRoofRailing()) optionalEquipmentSum += CarOptionalEquipment.ROOF_RAILING
                        if (it.getSecondKeySet() == true) optionalEquipmentSum += CarOptionalEquipment.SECOND_KEY_SET
                        if (it.getUserManual() == true) optionalEquipmentSum += CarOptionalEquipment.USER_MANUAL
                        if(it.getFloorMats() == true) optionalEquipmentSum += CarOptionalEquipment.FLOOR_MATS
                        // equipment is stored as logic sum of components(2^n)
                        optionalEquipment = optionalEquipmentSum

                        // Obligatory equipment
                        if (it.getRegistrationDocument()) obligatoryEquipmentSum += CarObligatoryEquipment.REGISTRATION_DOCUMENT
                        if (it.getOCInsurance()) obligatoryEquipmentSum += CarObligatoryEquipment.OC_INSURANCE
                        if (it.getFireExtinguisher()) obligatoryEquipmentSum += CarObligatoryEquipment.FIRE_EXTINGUISHER
                        if (it.getTriangle()) obligatoryEquipmentSum += CarObligatoryEquipment.TRIANGLE
                        if (it.getFirstAidKit()) obligatoryEquipmentSum += CarObligatoryEquipment.FIRST_AID_KIT
                        if (it.getKeys()) obligatoryEquipmentSum += CarObligatoryEquipment.KEYS
                        // equipment is stored as logic sum of components(2^n)
                        obligatoryEquipment = obligatoryEquipmentSum

                        // Caps
                        wheelCupsAmount = it.getCaps()   
                    }
                }
            }
        }
    }

    // save collected data and handle response
    private fun updateCarCondition() {

        composite.add(repository.updateCarCondition(updatedCondition)
            .subscribe(
                {
                    // success - navigate to next screen
                    RxBus.post(ChangeFragment(FragmentTag.COMMENTS))
                },
                { t ->
                    // error - display error message
                    getView()?.hideProgress()
                    RxBus.post(ErrorMessage(t, R.string.damages_update_error))
                }
            )
        )  
    }

    // save upholsery purity state to global persistence object
    fun markUpholsteryPurity(dirty: Boolean) {
        repository.carManager.dirtyUpholstery = dirty
    }

    // notify fragment to clear tires inputs of realized task is a transport car from dealer
    fun shouldClearTires(): Boolean {
        return (repository.carManager.isStarting && repository.carManager.currentTaskDetails?.type == Task.Type.TRANSPORT_CAR_FROM_DEALER) || (repository.carManager.isFinishing && repository.carManager.currentTaskDetails?.type == Task.Type.TRANSPORT_CAR_TO_DEALER)
    }

    override fun onAttachView() {}

    override fun onDetachView() {}

}
