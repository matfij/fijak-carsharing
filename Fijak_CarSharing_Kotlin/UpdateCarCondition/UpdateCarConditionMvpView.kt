package com.corcode.asystententerprise.UI.UpdateCarCondition

import com.corcode.asystententerprise.Base.MvpView

// contract between view and presenter that lets presenter know which method can be called on a view object (fragment)
interface UpdateCarConditionMvpView: MvpView {
    fun setTitle(titleID: Int)
    fun isRestored(): Boolean
    fun enableTiresEditing()
    fun setRegistrationNumberInput(registrationNumber: String?)
    fun isFormValid(): Boolean
    fun resetForm(clearRegistrationNo: Boolean)
    fun setupThirdPart(obligatoryEq: List<Int>, optionalEq: List<Int>, caps: Int?)
    fun setupSecondPart(cleanInside: Boolean, cleanOutside: Boolean, cleanUpholstery: Boolean, frontTiresType: Int?, frontTiresProducer: String?, frontTiresSize: String?, backTiresType: Int?, backTiresProducer: String?, backTiresSize: String?, frontWheelsType: Int?, backWheelsType: Int?)
    fun setupFirstPart(registrationNumber: String?, brandName: String?, modelName: String?, mileage: Int?, fuelState: Int?)
    fun observeUpholsteryPurity()
    fun getCourse(): Int
    fun getFuel(): Int
    fun getCleanInside(): Boolean
    fun getCleanOutside(): Boolean
    fun getCleanUpholstery(): Boolean
    fun getFrontTiresSummer(): Boolean
    fun getFrontTiresWinter(): Boolean
    fun getFrontTiresUniversal(): Boolean
    fun getBackTiresSummer(): Boolean
    fun getBackTiresWinter(): Boolean
    fun getBackTiresUniversal(): Boolean
    fun getFrontTiresManufacturer(): String
    fun getBackTiresManufacturer(): String
    fun getFrontTiresSize(): String
    fun getBackTiresSize(): String
    fun getFrontWheelsType(): Boolean
    fun getBackWheelsType(): Boolean
    fun checkSameAxles(): Boolean
    fun getAntenna(): Boolean
    fun getSecondKeySet(): Boolean?
    fun getUserManual(): Boolean?
    fun getFloorMats(): Boolean?
    fun getCaps(): Int
    fun getBlind(): Boolean
    fun getSpareWheel(): Boolean
    fun getJack(): Boolean
    fun getRepairKit(): Boolean
    fun getRoofRailing(): Boolean
    fun getGPS(): Boolean
    fun getFireExtinguisher(): Boolean
    fun getFirstAidKit(): Boolean
    fun getTriangle(): Boolean
    fun getRegistrationDocument(): Boolean
    fun getOCInsurance(): Boolean
    fun getKeys(): Boolean
    fun disableRegistrationNumberInput()
    fun showMileageLowerWarning()
    fun showMileageNotChangedWarning()
    fun showMileageTooHighWarning()
    fun hideBackAxle(checked: Boolean)
    fun showBackAxle()
    fun hideKeyboard()
    fun showProgress()
    fun hideProgress()
}
