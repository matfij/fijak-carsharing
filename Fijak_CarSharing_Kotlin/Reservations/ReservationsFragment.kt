package com.corcode.asystententerprise.UI.Reservations

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.corcode.asystententerprise.App
import com.corcode.asystententerprise.Bus.ChangeFragment
import com.corcode.asystententerprise.Bus.ProgressVisibility
import com.corcode.asystententerprise.Bus.RxBus
import com.corcode.asystententerprise.R
import com.corcode.asystententerprise.Schemas.Reservation
import com.corcode.asystententerprise.UI.Fleet.fleetsearch.FleetSearchDialog
import com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView
import com.corcode.asystententerprise.UI.Reservations.departmentsearch.DepartmentSearchDialog
import com.corcode.asystententerprise.UI.Reservations.departmentsearch.DepartmentSearchPresenter
import com.corcode.asystententerprise.Utils.FragmentTag
import com.corcode.asystententerprise.Utils.invisible
import com.corcode.asystententerprise.Utils.visible
import kotlinx.android.synthetic.main.fragment_reservations.*
import javax.inject.Inject



class ReservationsFragment: Fragment(), ReservationsView {

    // controller (in MVP architecture) for fetching/storing required data
    @Inject
    lateinit var presenter: ReservationsPresenter

    // adapter for populating reservation list into an android recyclerView
    private lateinit var adapter: ReservationsAdapter

    // view creation
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reservations, container, false)
        App.component.inject(this)
        presenter.attachView(this)
        (activity as NavigationMvpView).setToolbarTitle(R.string.reservation_list)
        setHasOptionsMenu(true)
        return view
    }

    // lifecycle hook which is executed automatically after view creation
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupEditingButton(false)
        presenter.start()
        presenter.registerForDepartmentSearchRequest()
        setupRecycler()
        setOnClickListeners()
    }

    // wiring up button click to presenter method
    private fun setOnClickListeners() {
        reservations_edit_description_button?.setOnClickListener {
            presenter.setFlagForNote(adapter.selectedReservation)
            RxBus.post(ChangeFragment(FragmentTag.COMMENTS))
        }
    }

    // method for enabling edit button if a reservation is selected
    override fun setupEditingButton(enabled: Boolean) {
        if(enabled) {
            reservations_edit_description_button?.isEnabled = true
            reservations_edit_description_button?.alpha = 1f
        } else {
            reservations_edit_description_button?.isEnabled = false
            reservations_edit_description_button?.alpha = 0.5f
        }
    }

    // reservaton list adapter for recyclerView intitialization 
    private fun setupRecycler() {
        adapter = ReservationsAdapter(presenter)
        reservations_recycler.layoutManager = LinearLayoutManager(context!!)
        reservations_recycler.adapter = adapter
    }
    override fun updateAdapter(orders: List<Reservation>) {
        adapter.updateData(orders)
    }
    override fun clearData() {
        adapter.clearData()
    }

    // build an alertDialog which will pop up if there is no planned reservation to display
    override fun showNoReservationError() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(getString(R.string.no_reservation_title))
        builder.setMessage(getString(R.string.no_reservation_error))
        builder.
                setNeutralButton(R.string.no_reservation_understood) {
                    _, _ ->
                }
        builder.show()
    }

    // show or hide progrss indicator during data transfers
    override fun showProgress() {
        reservations_recycler.invisible()
        RxBus.post(ProgressVisibility(true))
    }
    override fun hideProgress() {
        RxBus.post(ProgressVisibility(false))
        reservations_recycler.visible()
    }

}
