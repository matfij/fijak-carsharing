package com.corcode.asystententerprise.UI.Reservations

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.corcode.asystententerprise.R
import com.corcode.asystententerprise.Schemas.Reservation
import com.corcode.asystententerprise.Utils.Pattern
import com.corcode.asystententerprise.Utils.Utils
import com.corcode.asystententerprise.Utils.inflate
import kotlinx.android.synthetic.main.item_reservation.view.*

class ReservationsAdapter(private val presenter: ReservationsPresenter): RecyclerView.Adapter<ReservationsAdapter.ReservationHolder>() {

    // object for storing reservation that is currently selected by the user
    var selectedReservation: Reservation? = null

    // enumerable object for storing displayed reservation list
    private val reservations = mutableListOf<Reservation>()

    // method invoked from outside the adapter in which input reservation list is passed and stored 
    fun updateData(data: List<Reservation>) {
        reservations.clear()
        reservations.addAll(data)
    }

    // clear displayed reservation list
    fun clearData() {
        reservations.clear()
    }

    // lifecycle hook for connecting adapter to targeted layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReservationHolder =
            ReservationHolder(parent.inflate(R.layout.item_reservation))

    // lifecycle hook for displaying item on targeted layout (recyclerView)
    override fun onBindViewHolder(holder: ReservationHolder, position: Int) {
        holder.setupReservation(reservations[position])
    }

    // item metadata that is displayed inside single list entry
    inner class ReservationHolder(view: View) : RecyclerView.ViewHolder(view) {

        // flag which tells if any reservation from the displayed list is selected
        private var isSelected = false

        fun setupReservation(reservation: Reservation) = with(itemView) {

            // display forwarded data
            reservation.number.let { item_reservation_order_number?.text = it  }
            reservation.hireDateFrom.let { item_reservation_start_date?.text = Utils.getDateAsString(it, Pattern.DATE_HOUR) }
            reservation.returnDateTo.let { item_reservation_end_date?.text = Utils.getDateAsString(it, Pattern.DATE_HOUR) } }
            reservation.carRegistrationNumber.let { item_reservation_car_registration_number?.text = it?.get(0) } }
            reservation.clientName.let { item_reservation_client?.text = it }
            reservation.orderedAcriss.let { item_reservation_car_ordered_segment?.text = it }
            reservation.letOutAcriss.let { item_reservation_car_let_out_segment?.text = it }

            // Handle selection
            this.isSelected = reservation.selected

            // Assign colors
            val backgroundColor: Int
            val textColor: Int
            if (this.isSelected) {
                backgroundColor = R.color.brand_alt
                textColor = R.color.white
            } else {
                backgroundColor = R.color.white
                textColor = R.color.grey_dark
            }

            // Background
            item_reservation_order_number.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_start_date.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_end_date.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_car_registration_number.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_client.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_car_ordered_segment.background = ContextCompat.getDrawable(context!!, backgroundColor)
            item_reservation_car_let_out_segment.background = ContextCompat.getDrawable(context!!, backgroundColor)
            // Text color
            item_reservation_order_number.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_start_date.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_end_date.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_car_registration_number.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_client.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_car_ordered_segment.setTextColor(ContextCompat.getColor(context!!, textColor))
            item_reservation_car_let_out_segment.setTextColor(ContextCompat.getColor(context!!, textColor))

            // handle click on reservation list item
            setOnClickListener {
                when {
                    reservation.selected -> {
                        reservation.selected = false
                        selectedReservation = null
                    }
                    else -> {
                        reservations.map { it.selected = false }
                        selectedReservation = reservation
                        reservation.selected = true
                    }
                }
            }

            // enable edit button if any reservation in selected
            if(selectedReservation != null) {
                presenter.setupEditingButton(true)
            } else {
                presenter.setupEditingButton(false)
            }
        }

    }
}
