package com.corcode.asystententerprise.UI.Reservations

import android.content.DialogInterface
import com.corcode.asystententerprise.Base.MvpView
import com.corcode.asystententerprise.Schemas.Reservation

// viewmodel interface for providing information which method can be invoked by presenter
interface ReservationsView: MvpView {
    fun setupEditingButton(enabled: Boolean)
    fun updateAdapter(orders: List<Reservation>)
    fun showNoReservationError()
    fun showProgress()
    fun hideProgress()
    fun clearData()
}
