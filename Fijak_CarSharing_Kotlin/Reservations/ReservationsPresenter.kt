package com.corcode.asystententerprise.UI.Reservations

import com.corcode.asystententerprise.Base.BasePresenter
import com.corcode.asystententerprise.Bus.*
import com.corcode.asystententerprise.Persistance.Repository
import com.corcode.asystententerprise.R
import com.corcode.asystententerprise.Schemas.GetDepartmentsParams
import com.corcode.asystententerprise.Schemas.GetReservationParams
import com.corcode.asystententerprise.Schemas.Reservation
import com.corcode.asystententerprise.Utils.Pattern
import com.corcode.asystententerprise.Utils.Utils
import io.reactivex.disposables.CompositeDisposable
import org.joda.time.DateTime
import javax.inject.Inject

class ReservationsPresenter @Inject constructor(private val repository: Repository): BasePresenter<ReservationsView>() {

    // wrapper that will get rid of components used inside after they finish their job
    private val composite = CompositeDisposable()

    fun start() {
        // parameter for searching reservation with given properties values
        val params = GetReservationParams()
        params.Order = 2
        params.State = 1
        params.HireDepartmentId = repository.carManager.searchingDepartmentId
        params.StartDateFrom = Utils.getDateAsString(DateTime.now().withTimeAtStartOfDay().toDate(), Pattern.DATE_API)
        params.StartDateTo = Utils.getDateAsString(DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate(), Pattern.DATE_API)

        // make an api request through repository controller
        getView()?.showProgress()
        composite.add(repository.getReservation(params).subscribe(
                { response ->
                    // successfull but empty response
                    if (response.reservationList.isEmpty()) {
                        getView()?.showNoReservationError()
                        getView()?.hideProgress()
                    }
                    // successful response 
                    else {
                        val reservationsArray: MutableList<Reservation> = mutableListOf()
                        response.reservationList.foreach { it =>
                            reservationsArray.add(it)
                        }
                        val reservationsList: List<Reservation> = reservationsArray
                        getView()?.updateAdapter(reservationsList)
                        getView()?.hideProgress()
                    }
                },
                // request error
                { t ->
                    getView()?.hideProgress()
                    RxBus.post(ErrorMessage(t))
                }
        ))
    }

    // enable edit button if there is a selecter reservation
    fun setupEditingButton(enabled: Boolean) {
        getView()?.setupEditingButton(enabled)
    }

    // store selected reservation in a repository (global data manager and api client caller)
    fun setFlagForNote(reservation: Reservation?) {
        repository.carManager.selectedReservation = reservation
        repository.carManager.isNoteForReservation = true
    }

    // android fragment lifecycle hooks which may be used for initializing some data before dragment creation
    // or remove data after fragment destroy
    override fun onAttachView() {}
    override fun onDetachView() {}
}
