package com.corcode.asystententerprise.UI.UpdateCarCondition;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.corcode.asystententerprise.App;
import com.corcode.asystententerprise.Base.MvpView;
import com.corcode.asystententerprise.Bus.ProgressVisibility;
import com.corcode.asystententerprise.Bus.RxBus;
import com.corcode.asystententerprise.Bus.ShowMessage;
import com.corcode.asystententerprise.R.id;
import com.corcode.asystententerprise.UI.ManageOrder.ManageOrderFragment;
import com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView;
import com.corcode.asystententerprise.Utils.CarObligatoryEquipment;
import com.corcode.asystententerprise.Utils.CarOptionalEquipment;
import com.corcode.asystententerprise.Utils.Extensions;
import com.corcode.asystententerprise.Utils.Pattern;
import com.google.android.material.textfield.TextInputLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref.IntRef;
import kotlin.ranges.IntRange;
import kotlin.text.Regex;
import kotlin.text.StringsKt;
import org.jetbrains.anko.ToastsKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b%\n\u0002\u0010 \n\u0002\b\f\u0018\u0000 x2\u00020\u00012\u00020\u0002:\u0001xB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\f\u001a\u00020\u000bH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002J\b\u0010\u0013\u001a\u00020\u000bH\u0016J\b\u0010\u0014\u001a\u00020\u0011H\u0016J\b\u0010\u0015\u001a\u00020\u0011H\u0016J\b\u0010\u0016\u001a\u00020\u000bH\u0016J\b\u0010\u0017\u001a\u00020\u000bH\u0016J\b\u0010\u0018\u001a\u00020\u000bH\u0016J\b\u0010\u0019\u001a\u00020\u000bH\u0016J\b\u0010\u001a\u001a\u00020\u000bH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u000bH\u0016J\b\u0010\u001e\u001a\u00020\u000bH\u0016J\b\u0010\u001f\u001a\u00020\u000bH\u0016J\b\u0010 \u001a\u00020\u001cH\u0016J\b\u0010!\u001a\u00020\u000bH\u0016J\b\u0010\"\u001a\u00020\u000bH\u0016J\u000f\u0010#\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0002\u0010$J\b\u0010%\u001a\u00020\u0011H\u0016J\b\u0010&\u001a\u00020\u0011H\u0016J\b\u0010'\u001a\u00020\u000bH\u0016J\b\u0010(\u001a\u00020\u000bH\u0016J\b\u0010)\u001a\u00020\u000bH\u0016J\b\u0010*\u001a\u00020\u000bH\u0016J\b\u0010+\u001a\u00020\u001cH\u0016J\b\u0010,\u001a\u00020\u000bH\u0016J\b\u0010-\u001a\u00020\u000bH\u0016J\b\u0010.\u001a\u00020\u000bH\u0016J\b\u0010/\u001a\u00020\u000bH\u0016J\b\u00100\u001a\u00020\u000bH\u0016J\b\u00101\u001a\u00020\u000bH\u0016J\b\u00102\u001a\u00020\u000bH\u0016J\u000f\u00103\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0002\u0010$J\b\u00104\u001a\u00020\u000bH\u0016J\b\u00105\u001a\u00020\u000bH\u0016J\u000f\u00106\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0002\u0010$J\u0010\u00107\u001a\u00020\u000e2\u0006\u00108\u001a\u00020\u000bH\u0016J\b\u00109\u001a\u00020\u000eH\u0002J\b\u0010:\u001a\u00020\u000eH\u0016J\b\u0010;\u001a\u00020\u000eH\u0016J\b\u0010<\u001a\u00020\u000bH\u0016J\b\u0010=\u001a\u00020\u000bH\u0016J\b\u0010>\u001a\u00020\u000eH\u0016J\b\u0010?\u001a\u00020\u000eH\u0002J&\u0010@\u001a\u0004\u0018\u00010A2\u0006\u0010B\u001a\u00020C2\b\u0010D\u001a\u0004\u0018\u00010E2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J\b\u0010H\u001a\u00020\u000eH\u0016J\b\u0010I\u001a\u00020\u000eH\u0016J\u0010\u0010J\u001a\u00020\u000e2\u0006\u0010K\u001a\u00020GH\u0016J\u001a\u0010L\u001a\u00020\u000e2\u0006\u0010M\u001a\u00020A2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J\u0010\u0010N\u001a\u00020\u000e2\u0006\u0010O\u001a\u00020\u000bH\u0016J\u0012\u0010P\u001a\u00020\u000e2\b\u0010Q\u001a\u0004\u0018\u00010\u0011H\u0016J\u0010\u0010R\u001a\u00020\u000e2\u0006\u0010S\u001a\u00020\u001cH\u0016J\b\u0010T\u001a\u00020\u000eH\u0002J\b\u0010U\u001a\u00020\u000eH\u0002J?\u0010V\u001a\u00020\u000e2\b\u0010Q\u001a\u0004\u0018\u00010\u00112\b\u0010W\u001a\u0004\u0018\u00010\u00112\b\u0010X\u001a\u0004\u0018\u00010\u00112\b\u0010Y\u001a\u0004\u0018\u00010\u001c2\b\u0010Z\u001a\u0004\u0018\u00010\u001cH\u0016¢\u0006\u0002\u0010[J\b\u0010\\\u001a\u00020\u000eH\u0002J\b\u0010]\u001a\u00020\u000eH\u0002Ju\u0010^\u001a\u00020\u000e2\u0006\u0010_\u001a\u00020\u000b2\u0006\u0010`\u001a\u00020\u000b2\u0006\u0010a\u001a\u00020\u000b2\b\u0010b\u001a\u0004\u0018\u00010\u001c2\b\u0010c\u001a\u0004\u0018\u00010\u00112\b\u0010d\u001a\u0004\u0018\u00010\u00112\b\u0010e\u001a\u0004\u0018\u00010\u001c2\b\u0010f\u001a\u0004\u0018\u00010\u00112\b\u0010g\u001a\u0004\u0018\u00010\u00112\b\u0010h\u001a\u0004\u0018\u00010\u001c2\b\u0010i\u001a\u0004\u0018\u00010\u001cH\u0016¢\u0006\u0002\u0010jJ3\u0010k\u001a\u00020\u000e2\f\u0010l\u001a\b\u0012\u0004\u0012\u00020\u001c0m2\f\u0010n\u001a\b\u0012\u0004\u0012\u00020\u001c0m2\b\u0010o\u001a\u0004\u0018\u00010\u001cH\u0016¢\u0006\u0002\u0010pJ\b\u0010q\u001a\u00020\u000eH\u0016J\b\u0010r\u001a\u00020\u000eH\u0002J\b\u0010s\u001a\u00020\u000eH\u0016J\b\u0010t\u001a\u00020\u000eH\u0016J\b\u0010u\u001a\u00020\u000eH\u0016J\b\u0010v\u001a\u00020\u000eH\u0016J\b\u0010w\u001a\u00020\u000bH\u0002R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006y"},
   d2 = {"Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionMvpView;", "()V", "presenter", "Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionPresenter;", "getPresenter", "()Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionPresenter;", "setPresenter", "(Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionPresenter;)V", "restoredFragment", "", "checkSameAxles", "disableRegistrationNumberInput", "", "enableTiresEditing", "formatTiresSize", "", "size", "getAntenna", "getBackTiresManufacturer", "getBackTiresSize", "getBackTiresSummer", "getBackTiresUniversal", "getBackTiresWinter", "getBackWheelsType", "getBlind", "getCaps", "", "getCleanInside", "getCleanOutside", "getCleanUpholstery", "getCourse", "getFireExtinguisher", "getFirstAidKit", "getFloorMats", "()Ljava/lang/Boolean;", "getFrontTiresManufacturer", "getFrontTiresSize", "getFrontTiresSummer", "getFrontTiresUniversal", "getFrontTiresWinter", "getFrontWheelsType", "getFuel", "getGPS", "getJack", "getKeys", "getOCInsurance", "getRegistrationDocument", "getRepairKit", "getRoofRailing", "getSecondKeySet", "getSpareWheel", "getTriangle", "getUserManual", "hideBackAxle", "checked", "hideCarDetails", "hideKeyboard", "hideProgress", "isFormValid", "isRestored", "observeUpholsteryPurity", "observeWheelsType", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onResume", "onSaveInstanceState", "outState", "onViewCreated", "view", "resetForm", "clearRegistrationNo", "setRegistrationNumberInput", "registrationNumber", "setTitle", "titleID", "setupBackTiresTextChangeListener", "setupClickListeners", "setupFirstPart", "brandName", "modelName", "mileage", "fuelState", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V", "setupFrontTiresTextChangeListener", "setupSameAxlesChangeListener", "setupSecondPart", "cleanInside", "cleanOutside", "cleanUpholstery", "frontTiresType", "frontTiresProducer", "frontTiresSize", "backTiresType", "backTiresProducer", "backTiresSize", "frontWheelsType", "backWheelsType", "(ZZZLjava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V", "setupThirdPart", "obligatoryEq", "", "optionalEq", "caps", "(Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;)V", "showBackAxle", "showCarDetails", "showMileageLowerWarning", "showMileageNotChangedWarning", "showMileageTooHighWarning", "showProgress", "validateRequiredFields", "Companion", "app_debug"}
)
public final class UpdateCarConditionFragment extends Fragment implements UpdateCarConditionMvpView {
   @Inject
   @NotNull
   public UpdateCarConditionPresenter presenter;
   private boolean restoredFragment;
   @NotNull
   private static final String SAVED_STATE = "saved";
   public static final UpdateCarConditionFragment.Companion Companion = new UpdateCarConditionFragment.Companion((DefaultConstructorMarker)null);
   private HashMap _$_findViewCache;

   @NotNull
   public final UpdateCarConditionPresenter getPresenter() {
      UpdateCarConditionPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      return var10000;
   }

   public final void setPresenter(@NotNull UpdateCarConditionPresenter var1) {
      Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
      this.presenter = var1;
   }

   public void onSaveInstanceState(@NotNull Bundle outState) {
      Intrinsics.checkParameterIsNotNull(outState, "outState");
      outState.putBoolean(SAVED_STATE, this.restoredFragment);
      super.onSaveInstanceState(outState);
   }

   public boolean isRestored() {
      return this.restoredFragment;
   }

   @Nullable
   public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      Intrinsics.checkParameterIsNotNull(inflater, "inflater");
      if (savedInstanceState != null) {
         boolean var5 = false;
         boolean var6 = false;
         int var8 = false;
         this.restoredFragment = savedInstanceState.getBoolean(SAVED_STATE);
      }

      View view = inflater.inflate(-1300106, container, false);
      App.Companion.getComponent().inject(this);
      UpdateCarConditionPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.attachView((MvpView)this);
      FragmentActivity var9 = this.getActivity();
      if (var9 == null) {
         throw new TypeCastException("null cannot be cast to non-null type com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView");
      } else {
         ((NavigationMvpView)var9).setToolbarTitle(-1900195);
         return view;
      }
   }

   public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
      Intrinsics.checkParameterIsNotNull(view, "view");
      UpdateCarConditionPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.start();
      this.setupClickListeners();
      this.setupFrontTiresTextChangeListener();
      this.setupBackTiresTextChangeListener();
      this.setupSameAxlesChangeListener();
      this.observeWheelsType();
   }

   public void onResume() {
      super.onResume();
      UpdateCarConditionPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.start();
      if (!this.restoredFragment) {
         this.restoredFragment = true;
      }

   }

   public void onDestroyView() {
      UpdateCarConditionPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.detachView();
      super.onDestroyView();
      this._$_clearFindViewByIdCache();
   }

   public void setTitle(int titleID) {
      FragmentActivity var10000 = this.getActivity();
      if (var10000 == null) {
         throw new TypeCastException("null cannot be cast to non-null type com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView");
      } else {
         ((NavigationMvpView)var10000).setToolbarTitle(titleID);
      }
   }

   private final void setupClickListeners() {
      ((TextView)this._$_findCachedViewById(id.handcar_button_next)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            if (UpdateCarConditionFragment.this.validateRequiredFields()) {
               UpdateCarConditionFragment.this.getPresenter().onNextClick();
            }

         }
      }));
   }

   private final boolean validateRequiredFields() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_course_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_course_input");
      CharSequence var1 = (CharSequence)var10000.getText();
      boolean var2 = false;
      boolean var3 = false;
      boolean var5;
      if (var1 != null && !StringsKt.isBlank(var1)) {
         var10000 = (EditText)this._$_findCachedViewById(id.handcar_fuel_input);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_fuel_input");
         var1 = (CharSequence)var10000.getText();
         var2 = false;
         var3 = false;
         if (var1 != null && !StringsKt.isBlank(var1)) {
            var10000 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
            var1 = (CharSequence)var10000.getText();
            var2 = false;
            var3 = false;
            if (var1 != null && !StringsKt.isBlank(var1)) {
               var10000 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
               var1 = (CharSequence)var10000.getText();
               var2 = false;
               var3 = false;
               if (var1 != null && !StringsKt.isBlank(var1)) {
                  var5 = true;
                  return var5;
               }
            }
         }
      }

      Context var4 = this.getContext();
      if (var4 == null) {
         Intrinsics.throwNpe();
      }

      Intrinsics.checkExpressionValueIsNotNull(var4, "context!!");
      String var10001 = this.getString(-1900348);
      Intrinsics.checkExpressionValueIsNotNull(var10001, "getString(R.string.required_fields_error)");
      ToastsKt.toast(var4, (CharSequence)var10001);
      var5 = false;
      return var5;
   }

   public void enableTiresEditing() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_summer");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_winter");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_universal");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_steel);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_steel");
      var10000.setEnabled(true);
      EditText var1 = (EditText)this._$_findCachedViewById(id.handcar_front_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_front_manufacturer_input");
      var1.setEnabled(true);
      var1 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_front_tires_input");
      var1.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_summer");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_winter");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_universal");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_aluminum");
      var10000.setEnabled(true);
      var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_steel);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_steel");
      var10000.setEnabled(true);
      var1 = (EditText)this._$_findCachedViewById(id.handcar_back_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_back_manufacturer_input");
      var1.setEnabled(true);
      var1 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_back_tires_input");
      var1.setEnabled(true);
      CheckBox var2 = (CheckBox)this._$_findCachedViewById(id.sameAxlesCheckbox);
      Intrinsics.checkExpressionValueIsNotNull(var2, "sameAxlesCheckbox");
      var2.setEnabled(true);
   }

   public void observeUpholsteryPurity() {
      ((RadioButton)this._$_findCachedViewById(id.handcar_upholstery_dirty)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            UpdateCarConditionFragment.this.getPresenter().markUpholsteryPurity(true);
         }
      }));
      ((RadioButton)this._$_findCachedViewById(id.handcar_upholstery_clean)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            UpdateCarConditionFragment.this.getPresenter().markUpholsteryPurity(false);
         }
      }));
   }

   public void setRegistrationNumberInput(@Nullable String registrationNumber) {
      if (registrationNumber != null) {
         boolean var3 = false;
         boolean var4 = false;
         int var6 = false;
         EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_registration_number_input);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_number_input");
         var10000.setText((CharSequence)(new SpannableStringBuilder((CharSequence)registrationNumber)));
      }

   }

   public void disableRegistrationNumberInput() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_registration_number_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_number_input");
      var10000.setEnabled(false);
   }

   private final void setupSameAxlesChangeListener() {
      ((CheckBox)this._$_findCachedViewById(id.sameAxlesCheckbox)).setOnCheckedChangeListener((OnCheckedChangeListener)(new OnCheckedChangeListener() {
         public final void onCheckedChanged(CompoundButton $noName_0, boolean isChecked) {
            if (isChecked) {
               UpdateCarConditionFragment.this.hideBackAxle(false);
            } else {
               UpdateCarConditionFragment.this.showBackAxle();
            }

         }
      }));
   }

   private final void setupFrontTiresTextChangeListener() {
      final IntRef length = new IntRef();
      length.element = 0;
      ((EditText)this._$_findCachedViewById(id.handcar_front_tires_input)).addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(@Nullable Editable s) {
         }

         public void beforeTextChanged(@Nullable CharSequence s, int start, int count, int after) {
            IntRef var10000 = length;
            EditText var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
            var10000.element = var10001.getText().toString().length();
         }

         public void onTextChanged(@Nullable CharSequence s, int start, int before, int count) {
            EditText var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
            String text = var10000.getText().toString();
            EditText var10001;
            if ((text.length() != 3 || length.element >= text.length()) && (text.length() != 6 || length.element >= text.length())) {
               if (text.length() == 7 && length.element < text.length()) {
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
                  var10000.setText((CharSequence)(new SpannableStringBuilder((CharSequence)(new StringBuilder()).append(s).append('R').toString())));
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
                  var10000.setSelection(var10001.getText().length());
               } else if (text.length() > 10) {
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
                  byte var7 = 0;
                  byte var8 = 10;
                  EditText var10 = var10000;
                  boolean var9 = false;
                  if (text == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  String var15 = text.substring(var7, var8);
                  Intrinsics.checkExpressionValueIsNotNull(var15, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                  String var13 = var15;
                  CharSequence var14 = (CharSequence)var13;
                  var10.setText((CharSequence)(new SpannableStringBuilder(var14)));
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
                  var10000.setSelection(var10001.getText().length());
               }
            } else {
               var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
               var10000.setText((CharSequence)(new SpannableStringBuilder((CharSequence)(new StringBuilder()).append(s).append('/').toString())));
               var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
               var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_tires_input);
               Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
               var10000.setSelection(var10001.getText().length());
            }

         }
      }));
   }

   private final void setupBackTiresTextChangeListener() {
      final IntRef length = new IntRef();
      length.element = 0;
      ((EditText)this._$_findCachedViewById(id.handcar_back_tires_input)).addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(@Nullable Editable s) {
         }

         public void beforeTextChanged(@Nullable CharSequence s, int start, int count, int after) {
            IntRef var10000 = length;
            EditText var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_back_tires_input");
            var10000.element = var10001.getText().toString().length();
         }

         public void onTextChanged(@Nullable CharSequence s, int start, int before, int count) {
            EditText var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
            String text = var10000.getText().toString();
            EditText var10001;
            if ((text.length() != 3 || length.element >= text.length()) && (text.length() != 6 || length.element >= text.length())) {
               if (text.length() == 7 && length.element < text.length()) {
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
                  var10000.setText((CharSequence)(new SpannableStringBuilder((CharSequence)(new StringBuilder()).append(s).append('R').toString())));
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_back_tires_input");
                  var10000.setSelection(var10001.getText().length());
               } else if (text.length() > 10) {
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
                  byte var7 = 0;
                  byte var8 = 10;
                  EditText var10 = var10000;
                  boolean var9 = false;
                  if (text == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  String var15 = text.substring(var7, var8);
                  Intrinsics.checkExpressionValueIsNotNull(var15, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                  String var13 = var15;
                  CharSequence var14 = (CharSequence)var13;
                  var10.setText((CharSequence)(new SpannableStringBuilder(var14)));
                  var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
                  Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_back_tires_input");
                  var10000.setSelection(var10001.getText().length());
               }
            } else {
               var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
               var10000.setText((CharSequence)(new SpannableStringBuilder((CharSequence)(new StringBuilder()).append(s).append('/').toString())));
               var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
               var10001 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_tires_input);
               Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_back_tires_input");
               var10000.setSelection(var10001.getText().length());
            }

         }
      }));
   }

   public void hideBackAxle(boolean checked) {
      if (checked) {
         CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.sameAxlesCheckbox);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "sameAxlesCheckbox");
         var10000.setChecked(true);
      }

      TextView var2 = (TextView)this._$_findCachedViewById(id.handcar_back_tires_text);
      Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_back_tires_text");
      Extensions.gone((View)var2);
      RadioGroup var3 = (RadioGroup)this._$_findCachedViewById(id.handcar_back_tires_group);
      Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_back_tires_group");
      Extensions.gone((View)var3);
      TextInputLayout var4 = (TextInputLayout)this._$_findCachedViewById(id.handcar_back_manufacturer);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_back_manufacturer");
      Extensions.gone((View)var4);
      var4 = (TextInputLayout)this._$_findCachedViewById(id.handcar_back_tires_layout);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_back_tires_layout");
      Extensions.gone((View)var4);
      var3 = (RadioGroup)this._$_findCachedViewById(id.handcar_back_wheels_type);
      Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_back_wheels_type");
      Extensions.gone((View)var3);
      ((TextView)this._$_findCachedViewById(id.handcar_front_tires_text)).setText(-1900079);
      EditText var6 = (EditText)this._$_findCachedViewById(id.handcar_back_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var6, "handcar_back_manufacturer_input");
      EditText var10001 = (EditText)this._$_findCachedViewById(id.handcar_front_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_manufacturer_input");
      var6.setText((CharSequence)var10001.getText());
      var6 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var6, "handcar_back_tires_input");
      var10001 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
      var6.setText((CharSequence)var10001.getText());
      RadioButton var7 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var7, "handcar_back_tires_summer");
      RadioButton var5 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var5, "handcar_front_tires_summer");
      var7.setChecked(var5.isChecked());
      var7 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var7, "handcar_back_tires_winter");
      var5 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var5, "handcar_front_tires_winter");
      var7.setChecked(var5.isChecked());
      var7 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var7, "handcar_back_tires_universal");
      var5 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var5, "handcar_front_tires_universal");
      var7.setChecked(var5.isChecked());
      var7 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var7, "handcar_back_wheels_aluminum");
      var5 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var5, "handcar_front_wheels_aluminum");
      var7.setChecked(var5.isChecked());
      var7 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_steel);
      Intrinsics.checkExpressionValueIsNotNull(var7, "handcar_back_wheels_steel");
      var5 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_steel);
      Intrinsics.checkExpressionValueIsNotNull(var5, "handcar_front_wheels_steel");
      var7.setChecked(var5.isChecked());
   }

   public void showBackAxle() {
      TextView var10000 = (TextView)this._$_findCachedViewById(id.handcar_back_tires_text);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_text");
      Extensions.visible((View)var10000);
      RadioGroup var1 = (RadioGroup)this._$_findCachedViewById(id.handcar_back_tires_group);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_back_tires_group");
      Extensions.visible((View)var1);
      TextInputLayout var2 = (TextInputLayout)this._$_findCachedViewById(id.handcar_back_manufacturer);
      Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_back_manufacturer");
      Extensions.visible((View)var2);
      var2 = (TextInputLayout)this._$_findCachedViewById(id.handcar_back_tires_layout);
      Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_back_tires_layout");
      Extensions.visible((View)var2);
      var1 = (RadioGroup)this._$_findCachedViewById(id.handcar_back_wheels_type);
      Intrinsics.checkExpressionValueIsNotNull(var1, "handcar_back_wheels_type");
      Extensions.visible((View)var1);
      ((TextView)this._$_findCachedViewById(id.handcar_front_tires_text)).setText(-1900447);
   }

   private final void observeWheelsType() {
      ((RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_aluminum)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            label20: {
               RadioButton var10000 = (RadioButton)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
               if (var10000.isChecked()) {
                  var10000 = (RadioButton)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_aluminum");
                  if (var10000.isChecked()) {
                     break label20;
                  }
               }

               var10000 = (RadioButton)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
               if (!var10000.isChecked()) {
                  return;
               }

               CheckBox var2 = (CheckBox)UpdateCarConditionFragment.this._$_findCachedViewById(id.sameAxlesCheckbox);
               Intrinsics.checkExpressionValueIsNotNull(var2, "sameAxlesCheckbox");
               if (!var2.isChecked()) {
                  return;
               }
            }

            ((EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input)).setText((CharSequence)"0");
            EditText var3 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input);
            Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_caps_input");
            var3.setEnabled(false);
         }
      }));
      ((RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_steel)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            EditText var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_caps_input");
            var10000.setEnabled(true);
         }
      }));
      ((RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_aluminum)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            RadioButton var10000 = (RadioButton)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
            if (var10000.isChecked()) {
               var10000 = (RadioButton)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_aluminum");
               if (var10000.isChecked()) {
                  ((EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input)).setText((CharSequence)"0");
                  EditText var2 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input);
                  Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_caps_input");
                  var2.setEnabled(false);
               }
            }

         }
      }));
      ((RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_steel)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View it) {
            EditText var10000 = (EditText)UpdateCarConditionFragment.this._$_findCachedViewById(id.handcar_caps_input);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_caps_input");
            var10000.setEnabled(true);
         }
      }));
   }

   private final void showCarDetails() {
      TextView var10000 = (TextView)this._$_findCachedViewById(id.handcar_model_text);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model_text");
      Extensions.visible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_model);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model");
      Extensions.visible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_registration_no_text);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_no_text");
      Extensions.visible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_registration_no);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_no");
      Extensions.visible((View)var10000);
   }

   private final void hideCarDetails() {
      TextView var10000 = (TextView)this._$_findCachedViewById(id.handcar_model_text);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model_text");
      Extensions.invisible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_model);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model");
      Extensions.invisible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_registration_no_text);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_no_text");
      Extensions.invisible((View)var10000);
      var10000 = (TextView)this._$_findCachedViewById(id.handcar_registration_no);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_no");
      Extensions.invisible((View)var10000);
   }

   public void setupFirstPart(@Nullable String registrationNumber, @Nullable String brandName, @Nullable String modelName, @Nullable Integer mileage, @Nullable Integer fuelState) {
      boolean var7;
      boolean var8;
      boolean var10;
      TextView var10000;
      if (registrationNumber != null) {
         var7 = false;
         var8 = false;
         var10 = false;
         var10000 = (TextView)this._$_findCachedViewById(id.handcar_registration_no);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_no");
         var10000.setText((CharSequence)registrationNumber);
      }

      if (brandName != null) {
         var7 = false;
         var8 = false;
         var10 = false;
         var10000 = (TextView)this._$_findCachedViewById(id.handcar_model);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model");
         var10000.setText((CharSequence)(brandName + ' '));
      }

      if (modelName != null) {
         var7 = false;
         var8 = false;
         var10 = false;
         var10000 = (TextView)this._$_findCachedViewById(id.handcar_model);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model");
         CharSequence brand = var10000.getText();
         var10000 = (TextView)this._$_findCachedViewById(id.handcar_model);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_model");
         var10000.setText((CharSequence)(new StringBuilder()).append(brand).append(' ').append(modelName).toString());
      }

      int it;
      EditText var12;
      if (mileage != null) {
         var7 = false;
         var8 = false;
         it = ((Number)mileage).intValue();
         var10 = false;
         var12 = (EditText)this._$_findCachedViewById(id.handcar_course_input);
         Intrinsics.checkExpressionValueIsNotNull(var12, "handcar_course_input");
         var12.setText((CharSequence)(new SpannableStringBuilder((CharSequence)String.valueOf(mileage))));
      }

      if (fuelState != null) {
         var7 = false;
         var8 = false;
         it = ((Number)fuelState).intValue();
         var10 = false;
         var12 = (EditText)this._$_findCachedViewById(id.handcar_fuel_input);
         Intrinsics.checkExpressionValueIsNotNull(var12, "handcar_fuel_input");
         var12.setText((CharSequence)(new SpannableStringBuilder((CharSequence)String.valueOf(fuelState))));
      }

      this.showCarDetails();
   }

   public void setupSecondPart(boolean cleanInside, boolean cleanOutside, boolean cleanUpholstery, @Nullable Integer frontTiresType, @Nullable String frontTiresProducer, @Nullable String frontTiresSize, @Nullable Integer backTiresType, @Nullable String backTiresProducer, @Nullable String backTiresSize, @Nullable Integer frontWheelsType, @Nullable Integer backWheelsType) {
      RadioButton var10000;
      if (cleanInside) {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_interior_clean);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_interior_clean");
         var10000.setChecked(true);
      } else {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_interior_dirty);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_interior_dirty");
         var10000.setChecked(true);
      }

      if (cleanOutside) {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_exterior_clean);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_exterior_clean");
         var10000.setChecked(true);
      } else {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_exterior_dirty);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_exterior_dirty");
         var10000.setChecked(true);
      }

      if (cleanUpholstery) {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_upholstery_clean);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_upholstery_clean");
         var10000.setChecked(true);
      } else {
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_upholstery_dirty);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_upholstery_dirty");
         var10000.setChecked(true);
      }

      UpdateCarConditionPresenter var18 = this.presenter;
      if (var18 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      if (var18.shouldClearTires()) {
         this.showBackAxle();
         CheckBox var19 = (CheckBox)this._$_findCachedViewById(id.sameAxlesCheckbox);
         Intrinsics.checkExpressionValueIsNotNull(var19, "sameAxlesCheckbox");
         var19.setChecked(false);
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_universal);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_universal");
         var10000.setChecked(true);
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_universal);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_universal");
         var10000.setChecked(true);
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_steel);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_steel");
         var10000.setChecked(true);
         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_steel);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_steel");
         var10000.setChecked(true);
      } else {
         boolean var13;
         byte var17;
         label113: {
            var13 = false;
            if (frontTiresType != null) {
               if (frontTiresType == 0) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_summer);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_summer");
                  var10000.setChecked(true);
                  break label113;
               }
            }

            var17 = 1;
            if (frontTiresType != null) {
               if (frontTiresType == var17) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_winter);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_winter");
                  var10000.setChecked(true);
                  break label113;
               }
            }

            var17 = 2;
            if (frontTiresType != null) {
               if (frontTiresType == var17) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_universal);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_universal");
                  var10000.setChecked(true);
               }
            }
         }

         boolean var14;
         boolean var16;
         EditText var20;
         if (frontTiresProducer != null) {
            var13 = false;
            var14 = false;
            var16 = false;
            var20 = (EditText)this._$_findCachedViewById(id.handcar_front_manufacturer_input);
            Intrinsics.checkExpressionValueIsNotNull(var20, "handcar_front_manufacturer_input");
            var20.setText((CharSequence)(new SpannableStringBuilder((CharSequence)frontTiresProducer)));
         }

         if (frontTiresSize != null) {
            var13 = false;
            var14 = false;
            var16 = false;
            var20 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var20, "handcar_front_tires_input");
            var20.setText((CharSequence)(new SpannableStringBuilder((CharSequence)this.formatTiresSize(frontTiresSize))));
         }

         byte var12;
         label97: {
            var12 = 2;
            if (frontWheelsType != null) {
               if (frontWheelsType == var12) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
                  var10000.setChecked(true);
                  break label97;
               }
            }

            var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_steel);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_steel");
            var10000.setChecked(true);
         }

         label114: {
            var13 = false;
            if (backTiresType != null) {
               if (backTiresType == 0) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_summer);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_summer");
                  var10000.setChecked(true);
                  break label114;
               }
            }

            var17 = 1;
            if (backTiresType != null) {
               if (backTiresType == var17) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_winter);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_winter");
                  var10000.setChecked(true);
                  break label114;
               }
            }

            var17 = 2;
            if (backTiresType != null) {
               if (backTiresType == var17) {
                  var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_universal);
                  Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_universal");
                  var10000.setChecked(true);
               }
            }
         }

         if (backTiresProducer != null) {
            var13 = false;
            var14 = false;
            var16 = false;
            var20 = (EditText)this._$_findCachedViewById(id.handcar_back_manufacturer_input);
            Intrinsics.checkExpressionValueIsNotNull(var20, "handcar_back_manufacturer_input");
            var20.setText((CharSequence)(new SpannableStringBuilder((CharSequence)backTiresProducer)));
         }

         if (backTiresSize != null) {
            var13 = false;
            var14 = false;
            var16 = false;
            var20 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
            Intrinsics.checkExpressionValueIsNotNull(var20, "handcar_back_tires_input");
            var20.setText((CharSequence)(new SpannableStringBuilder((CharSequence)this.formatTiresSize(backTiresSize))));
         }

         var12 = 2;
         if (backWheelsType != null) {
            if (backWheelsType == var12) {
               var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
               Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_aluminum");
               var10000.setChecked(true);
               return;
            }
         }

         var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_steel);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_steel");
         var10000.setChecked(true);
      }

   }

   private final String formatTiresSize(String size) {
      String var10000;
      if (size != null) {
         if (size.length() == 10) {
            var10000 = size;
            return var10000;
         }
      }

      String first;
      try {
         first = size != null ? StringsKt.substring(size, new IntRange(0, 2)) : null;
         String second = size != null ? StringsKt.substring(size, new IntRange(3, 4)) : null;
         String third = 'R' + (size != null ? StringsKt.substring(size, new IntRange(5, 6)) : null);
         first = CollectionsKt.joinToString$default((Iterable)CollectionsKt.listOf(new String[]{first, second, third}), (CharSequence)"/", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null);
      } catch (Throwable var5) {
         first = size;
      }

      var10000 = first;
      return var10000;
   }

   public void setupThirdPart(@NotNull List obligatoryEq, @NotNull List optionalEq, @Nullable Integer caps) {
      Intrinsics.checkParameterIsNotNull(obligatoryEq, "obligatoryEq");
      Intrinsics.checkParameterIsNotNull(optionalEq, "optionalEq");
      Iterable $this$forEach$iv = (Iterable)obligatoryEq;
      int $i$f$forEach = false;
      Iterator var6 = $this$forEach$iv.iterator();

      Object element$iv;
      int i;
      boolean var9;
      CheckBox var10000;
      while(var6.hasNext()) {
         element$iv = var6.next();
         i = ((Number)element$iv).intValue();
         var9 = false;
         if (i == CarObligatoryEquipment.FIRE_EXTINGUISHER) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_extinguisher);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_extinguisher");
            var10000.setChecked(true);
         } else if (i == CarObligatoryEquipment.FIRST_AID_KIT) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_first_aid_kit);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_first_aid_kit");
            var10000.setChecked(true);
         } else if (i == CarObligatoryEquipment.TRIANGLE) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_triangle);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_triangle");
            var10000.setChecked(true);
         } else if (i == CarObligatoryEquipment.REGISTRATION_DOCUMENT) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_registration_document);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_document");
            var10000.setChecked(true);
         } else if (i == CarObligatoryEquipment.OC_INSURANCE) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_oc_insurance);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_oc_insurance");
            var10000.setChecked(true);
         } else if (i == CarObligatoryEquipment.KEYS) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_keys);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_keys");
            var10000.setChecked(true);
         }
      }

      $this$forEach$iv = (Iterable)optionalEq;
      $i$f$forEach = false;
      var6 = $this$forEach$iv.iterator();

      while(var6.hasNext()) {
         element$iv = var6.next();
         i = ((Number)element$iv).intValue();
         var9 = false;
         if (i == CarOptionalEquipment.AERIAL) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_antenna);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_antenna");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.BOOT_COVER) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_blind);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_blind");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.CAR_LIFTING_TOOLS) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_jack);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_jack");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.GPS) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_gps);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_gps");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.SPARE_WHEEL) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_spare_wheel);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_spare_wheel");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.TIRE_REPAIR_KIT) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_repair_kit);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_repair_kit");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.ROOF_RAILING) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_roof_railing);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_roof_railing");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.SECOND_KEY_SET) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_second_key_set);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_second_key_set");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.USER_MANUAL) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_user_manual);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_user_manual");
            var10000.setChecked(true);
         } else if (i == CarOptionalEquipment.FLOOR_MATS) {
            var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_floor_mats);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_floor_mats");
            var10000.setChecked(true);
         }
      }

      UpdateCarConditionPresenter var14 = this.presenter;
      if (var14 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      if (var14.shouldClearTires()) {
         ((EditText)this._$_findCachedViewById(id.handcar_caps_input)).setText((CharSequence)"0");
      } else if (caps != null) {
         $i$f$forEach = false;
         boolean var11 = false;
         int it = ((Number)caps).intValue();
         int var13 = false;
         EditText var15 = (EditText)this._$_findCachedViewById(id.handcar_caps_input);
         Intrinsics.checkExpressionValueIsNotNull(var15, "handcar_caps_input");
         var15.setText((CharSequence)(new SpannableStringBuilder((CharSequence)String.valueOf(caps))));
      }

   }

   public boolean isFormValid() {
      Regex var10000 = Pattern.INSTANCE.getTIRE_SIZE();
      EditText var10001 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_front_tires_input");
      boolean var1;
      if (var10000.matches((CharSequence)var10001.getText().toString())) {
         var10000 = Pattern.INSTANCE.getTIRE_SIZE();
         var10001 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
         Intrinsics.checkExpressionValueIsNotNull(var10001, "handcar_back_tires_input");
         if (var10000.matches((CharSequence)var10001.getText().toString())) {
            var1 = true;
            return var1;
         }
      }

      RxBus.INSTANCE.post(new ShowMessage(-1900009, (String)null, 2, (DefaultConstructorMarker)null));
      var1 = false;
      return var1;
   }

   public void showMileageLowerWarning() {
      Builder var10000 = new Builder();
      Context var10002 = this.getContext();
      if (var10002 == null) {
         Intrinsics.throwNpe();
      }

      Builder builder = var10000;
      String var4 = this.getString(-1900089);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_…er_mileage_warning_title)");
      String title = var4;
      builder.setTitle((CharSequence)title);
      var4 = this.getString(-1900082);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_car_mileage_warning)");
      String message = var4;
      builder.setMessage((CharSequence)message);
      builder.setNeutralButton(-1900368, (android.content.DialogInterface.OnClickListener)null.INSTANCE);
      builder.show();
   }

   public void showMileageNotChangedWarning() {
      Builder var10000 = new Builder();
      Context var10002 = this.getContext();
      if (var10002 == null) {
         Intrinsics.throwNpe();
      }

      Builder builder = var10000;
      String var4 = this.getString(-1900034);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_…al_mileage_warning_title)");
      String title = var4;
      builder.setTitle((CharSequence)title);
      var4 = this.getString(-1900082);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_car_mileage_warning)");
      String message = var4;
      builder.setMessage((CharSequence)message);
      builder.setPositiveButton(-1900368, (android.content.DialogInterface.OnClickListener)(new android.content.DialogInterface.OnClickListener() {
         public final void onClick(DialogInterface $noName_0, int $noName_1) {
            UpdateCarConditionFragment.this.getPresenter().doNext();
         }
      }));
      builder.setNegativeButton(-1900000, (android.content.DialogInterface.OnClickListener)null.INSTANCE);
      builder.show();
   }

   public void showMileageTooHighWarning() {
      Builder var10000 = new Builder;
      Context var10002 = this.getContext();
      if (var10002 == null) {
         Intrinsics.throwNpe();
      }

      var10000.<init>(var10002);
      Builder builder = var10000;
      String var4 = this.getString(-1900362);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_…er_mileage_warning_title)");
      String title = var4;
      builder.setTitle((CharSequence)title);
      var4 = this.getString(-1900082);
      Intrinsics.checkExpressionValueIsNotNull(var4, "getString(R.string.hand_car_mileage_warning)");
      String message = var4;
      builder.setMessage((CharSequence)message);
      builder.setPositiveButton(-1900368, (android.content.DialogInterface.OnClickListener)(new android.content.DialogInterface.OnClickListener() {
         public final void onClick(DialogInterface $noName_0, int $noName_1) {
            UpdateCarConditionFragment.this.getPresenter().doNext();
         }
      }));
      builder.setNegativeButton(-1900000, (android.content.DialogInterface.OnClickListener)null.INSTANCE);
      builder.show();
   }

   public int getCourse() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_course_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_course_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         var1 = StringsKt.trim((CharSequence)var1).toString();
         var2 = false;
         return Integer.parseInt(var1);
      }
   }

   public int getFuel() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_fuel_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_fuel_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         var1 = StringsKt.trim((CharSequence)var1).toString();
         var2 = false;
         return Integer.parseInt(var1);
      }
   }

   public boolean getCleanInside() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_interior_clean);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_interior_clean");
      return var10000.isChecked();
   }

   public boolean getCleanOutside() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_exterior_clean);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_exterior_clean");
      return var10000.isChecked();
   }

   public boolean getCleanUpholstery() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_upholstery_clean);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_upholstery_clean");
      return var10000.isChecked();
   }

   public boolean getFrontTiresSummer() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_summer");
      return var10000.isChecked();
   }

   public boolean getFrontTiresWinter() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_winter");
      return var10000.isChecked();
   }

   public boolean getFrontTiresUniversal() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_universal");
      return var10000.isChecked();
   }

   public boolean getBackTiresSummer() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_summer);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_summer");
      return var10000.isChecked();
   }

   public boolean getBackTiresWinter() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_winter");
      return var10000.isChecked();
   }

   public boolean getBackTiresUniversal() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_tires_universal);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_universal");
      return var10000.isChecked();
   }

   @NotNull
   public String getFrontTiresManufacturer() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_front_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_manufacturer_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return StringsKt.trim((CharSequence)var1).toString();
      }
   }

   @NotNull
   public String getBackTiresManufacturer() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_back_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_manufacturer_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return StringsKt.trim((CharSequence)var1).toString();
      }
   }

   @NotNull
   public String getFrontTiresSize() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return StringsKt.trim((CharSequence)var1).toString();
      }
   }

   public boolean getFrontWheelsType() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_front_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_wheels_aluminum");
      return var10000.isChecked();
   }

   public boolean getBackWheelsType() {
      RadioButton var10000 = (RadioButton)this._$_findCachedViewById(id.handcar_back_wheels_aluminum);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_wheels_aluminum");
      return var10000.isChecked();
   }

   @NotNull
   public String getBackTiresSize() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      if (var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return StringsKt.trim((CharSequence)var1).toString();
      }
   }

   public boolean checkSameAxles() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.sameAxlesCheckbox);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "sameAxlesCheckbox");
      return var10000.isChecked();
   }

   public boolean getAntenna() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_antenna);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_antenna");
      return var10000.isChecked();
   }

   @Nullable
   public Boolean getSecondKeySet() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_second_key_set);
      return var10000 != null ? var10000.isChecked() : null;
   }

   @Nullable
   public Boolean getUserManual() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_user_manual);
      return var10000 != null ? var10000.isChecked() : null;
   }

   @Nullable
   public Boolean getFloorMats() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_floor_mats);
      return var10000 != null ? var10000.isChecked() : null;
   }

   public int getCaps() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_caps_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_caps_input");
      String var1 = var10000.getText().toString();
      boolean var2 = false;
      return Integer.parseInt(var1);
   }

   public boolean getBlind() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_blind);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_blind");
      return var10000.isChecked();
   }

   public boolean getSpareWheel() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_spare_wheel);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_spare_wheel");
      return var10000.isChecked();
   }

   public boolean getJack() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_jack);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_jack");
      return var10000.isChecked();
   }

   public boolean getRepairKit() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_repair_kit);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_repair_kit");
      return var10000.isChecked();
   }

   public boolean getGPS() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_gps);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_gps");
      return var10000.isChecked();
   }

   public boolean getRoofRailing() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_roof_railing);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_roof_railing");
      return var10000.isChecked();
   }

   public boolean getFireExtinguisher() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_extinguisher);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_extinguisher");
      return var10000.isChecked();
   }

   public boolean getFirstAidKit() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_first_aid_kit);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_first_aid_kit");
      return var10000.isChecked();
   }

   public boolean getTriangle() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_triangle);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_triangle");
      return var10000.isChecked();
   }

   public boolean getRegistrationDocument() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_registration_document);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_document");
      return var10000.isChecked();
   }

   public boolean getOCInsurance() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_oc_insurance);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_oc_insurance");
      return var10000.isChecked();
   }

   public boolean getKeys() {
      CheckBox var10000 = (CheckBox)this._$_findCachedViewById(id.handcar_keys);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_keys");
      return var10000.isChecked();
   }

   public void resetForm(boolean clearRegistrationNo) {
      EditText var10000;
      if (clearRegistrationNo) {
         var10000 = (EditText)this._$_findCachedViewById(id.handcar_registration_number_input);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_number_input");
         var10000.getText().clear();
      }

      TextView var2 = (TextView)this._$_findCachedViewById(id.handcar_model);
      Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_model");
      var2.setText((CharSequence)null);
      var2 = (TextView)this._$_findCachedViewById(id.handcar_registration_no);
      Intrinsics.checkExpressionValueIsNotNull(var2, "handcar_registration_no");
      var2.setText((CharSequence)null);
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_course_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_course_input");
      var10000.getText().clear();
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_fuel_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_fuel_input");
      var10000.getText().clear();
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_front_manufacturer_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_manufacturer_input");
      var10000.getText().clear();
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_front_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_front_tires_input");
      var10000.getText().clear();
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_back_tires_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_back_tires_input");
      var10000.getText().clear();
      var10000 = (EditText)this._$_findCachedViewById(id.handcar_caps_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_caps_input");
      var10000.getText().clear();
      RadioButton var3 = (RadioButton)this._$_findCachedViewById(id.handcar_interior_dirty);
      Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_interior_dirty");
      var3.setChecked(true);
      var3 = (RadioButton)this._$_findCachedViewById(id.handcar_exterior_dirty);
      Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_exterior_dirty");
      var3.setChecked(true);
      var3 = (RadioButton)this._$_findCachedViewById(id.handcar_front_tires_winter);
      Intrinsics.checkExpressionValueIsNotNull(var3, "handcar_front_tires_winter");
      var3.setChecked(true);
      CheckBox var4 = (CheckBox)this._$_findCachedViewById(id.handcar_antenna);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_antenna");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_blind);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_blind");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_spare_wheel);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_spare_wheel");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_extinguisher);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_extinguisher");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_first_aid_kit);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_first_aid_kit");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_triangle);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_triangle");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_jack);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_jack");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_repair_kit);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_repair_kit");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_gps);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_gps");
      var4.setChecked(false);
      var4 = (CheckBox)this._$_findCachedViewById(id.handcar_roof_railing);
      Intrinsics.checkExpressionValueIsNotNull(var4, "handcar_roof_railing");
      var4.setChecked(false);
   }

   public void hideKeyboard() {
      EditText var10000 = (EditText)this._$_findCachedViewById(id.handcar_registration_number_input);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_registration_number_input");
      View var1 = (View)var10000;
      Context var10001 = this.getContext();
      if (var10001 == null) {
         Intrinsics.throwNpe();
      }

      Intrinsics.checkExpressionValueIsNotNull(var10001, "context!!");
      Extensions.hideKeyboard(var1, var10001);
   }

   public void showProgress() {
      ConstraintLayout var10000 = (ConstraintLayout)this._$_findCachedViewById(id.handcar_layout);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_layout");
      Extensions.invisible((View)var10000);
      RxBus.INSTANCE.post(new ProgressVisibility(true));
   }

   public void hideProgress() {
      ConstraintLayout var10000 = (ConstraintLayout)this._$_findCachedViewById(id.handcar_layout);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "handcar_layout");
      Extensions.visible((View)var10000);
      RxBus.INSTANCE.post(new ProgressVisibility(false));
   }

   public View _$_findCachedViewById(int var1) {
      if (this._$_findViewCache == null) {
         this._$_findViewCache = new HashMap();
      }

      View var2 = (View)this._$_findViewCache.get(var1);
      if (var2 == null) {
         View var10000 = this.getView();
         if (var10000 == null) {
            return null;
         }

         var2 = var10000.findViewById(var1);
         this._$_findViewCache.put(var1, var2);
      }

      return var2;
   }

   public void _$_clearFindViewByIdCache() {
      if (this._$_findViewCache != null) {
         this._$_findViewCache.clear();
      }

   }

   @Metadata(
      mv = {1, 1, 15},
      bv = {1, 0, 3},
      k = 1,
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
      d2 = {"Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionFragment$Companion;", "", "()V", "SAVED_STATE", "", "getSAVED_STATE", "()Ljava/lang/String;", "newInstance", "Lcom/corcode/asystententerprise/UI/ManageOrder/ManageOrderFragment;", "bundle", "Landroid/os/Bundle;", "app_debug"}
   )
   public static final class Companion {
      @NotNull
      public final String getSAVED_STATE() {
         return UpdateCarConditionFragment.SAVED_STATE;
      }

      @NotNull
      public final ManageOrderFragment newInstance(@Nullable Bundle bundle) {
         ManageOrderFragment fragment = new ManageOrderFragment();
         fragment.setArguments(bundle);
         return fragment;
      }

      private Companion() {
      }

      // $FF: synthetic method
      public Companion(DefaultConstructorMarker $constructor_marker) {
         this();
      }
   }
}
