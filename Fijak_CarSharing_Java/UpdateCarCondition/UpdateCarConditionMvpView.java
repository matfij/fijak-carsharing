package com.corcode.asystententerprise.UI.UpdateCarCondition;

import com.corcode.asystententerprise.Base.MvpView;
import java.util.List;
import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface UpdateCarConditionMvpView extends MvpView {
   void setTitle(int var1);

   boolean isRestored();

   void enableTiresEditing();

   void setRegistrationNumberInput(@Nullable String var1);

   boolean isFormValid();

   void resetForm(boolean var1);

   void setupThirdPart(@NotNull List var1, @NotNull List var2, @Nullable Integer var3);

   void setupSecondPart(boolean var1, boolean var2, boolean var3, @Nullable Integer var4, @Nullable String var5, @Nullable String var6, @Nullable Integer var7, @Nullable String var8, @Nullable String var9, @Nullable Integer var10, @Nullable Integer var11);

   void setupFirstPart(@Nullable String var1, @Nullable String var2, @Nullable String var3, @Nullable Integer var4, @Nullable Integer var5);

   void observeUpholsteryPurity();

   int getCourse();

   int getFuel();

   boolean getCleanInside();

   boolean getCleanOutside();

   boolean getCleanUpholstery();

   boolean getFrontTiresSummer();

   boolean getFrontTiresWinter();

   boolean getFrontTiresUniversal();

   boolean getBackTiresSummer();

   boolean getBackTiresWinter();

   boolean getBackTiresUniversal();

   @NotNull
   String getFrontTiresManufacturer();

   @NotNull
   String getBackTiresManufacturer();

   @NotNull
   String getFrontTiresSize();

   @NotNull
   String getBackTiresSize();

   boolean getFrontWheelsType();

   boolean getBackWheelsType();

   boolean checkSameAxles();

   boolean getAntenna();

   @Nullable
   Boolean getSecondKeySet();

   @Nullable
   Boolean getUserManual();

   @Nullable
   Boolean getFloorMats();

   int getCaps();

   boolean getBlind();

   boolean getSpareWheel();

   boolean getJack();

   boolean getRepairKit();

   boolean getRoofRailing();

   boolean getGPS();

   boolean getFireExtinguisher();

   boolean getFirstAidKit();

   boolean getTriangle();

   boolean getRegistrationDocument();

   boolean getOCInsurance();

   boolean getKeys();

   void disableRegistrationNumberInput();

   void showMileageLowerWarning();

   void showMileageNotChangedWarning();

   void showMileageTooHighWarning();

   void hideBackAxle(boolean var1);

   void showBackAxle();

   void hideKeyboard();

   void showProgress();

   void hideProgress();
}
