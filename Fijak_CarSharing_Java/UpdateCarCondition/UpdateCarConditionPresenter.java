package com.corcode.asystententerprise.UI.UpdateCarCondition;

import com.corcode.asystententerprise.Base.BasePresenter;
import com.corcode.asystententerprise.Bus.ChangeFragment;
import com.corcode.asystententerprise.Bus.ErrorMessage;
import com.corcode.asystententerprise.Bus.RxBus;
import com.corcode.asystententerprise.Bus.ShowMain;
import com.corcode.asystententerprise.Bus.ShowMessage;
import com.corcode.asystententerprise.Managers.CarManager;
import com.corcode.asystententerprise.Persistance.Repository;
import com.corcode.asystententerprise.Schemas.CarData;
import com.corcode.asystententerprise.Schemas.CurrentCondition;
import com.corcode.asystententerprise.Schemas.FinishTaskParams;
import com.corcode.asystententerprise.Schemas.Task;
import com.corcode.asystententerprise.Schemas.TaskDetails;
import com.corcode.asystententerprise.Schemas.Tires;
import com.corcode.asystententerprise.Schemas.Task.Type;
import com.corcode.asystententerprise.Utils.BitUtils;
import com.corcode.asystententerprise.Utils.CarObligatoryEquipment;
import com.corcode.asystententerprise.Utils.CarOptionalEquipment;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import retrofit2.Response;

@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\fH\u0002J\u0006\u0010\r\u001a\u00020\u000bJ\b\u0010\u000e\u001a\u00020\u000bH\u0002J\b\u0010\u000f\u001a\u00020\u000bH\u0002J\u000e\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u0012J\b\u0010\u0013\u001a\u00020\u000bH\u0014J\b\u0010\u0014\u001a\u00020\u000bH\u0014J\u0006\u0010\u0015\u001a\u00020\u000bJ\b\u0010\u0016\u001a\u00020\u000bH\u0002J\u0012\u0010\u0017\u001a\u00020\u000b2\b\b\u0002\u0010\u0018\u001a\u00020\u0012H\u0002J\u0006\u0010\u0019\u001a\u00020\u0012J\u0006\u0010\u001a\u001a\u00020\u000bJ\b\u0010\u001b\u001a\u00020\u000bH\u0002R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
   d2 = {"Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionPresenter;", "Lcom/corcode/asystententerprise/Base/BasePresenter;", "Lcom/corcode/asystententerprise/UI/UpdateCarCondition/UpdateCarConditionMvpView;", "repository", "Lcom/corcode/asystententerprise/Persistance/Repository;", "(Lcom/corcode/asystententerprise/Persistance/Repository;)V", "car", "Lcom/corcode/asystententerprise/Schemas/CarData;", "composite", "Lio/reactivex/disposables/CompositeDisposable;", "checkAxles", "", "Lcom/corcode/asystententerprise/Schemas/CurrentCondition;", "doNext", "getInputsAndUpdateCarCondition", "isFormValid", "markUpholsteryPurity", "dirty", "", "onAttachView", "onDetachView", "onNextClick", "reassignCarData", "setCarData", "refreshRegistration", "shouldClearTires", "start", "updateCarCondition", "app_debug"}
)
public final class UpdateCarConditionPresenter extends BasePresenter {
   private CarData car;
   private final CompositeDisposable composite;
   private final Repository repository;

   public final void start() {
      this.car = this.repository.getCarManager().getCarData();
      UpdateCarConditionMvpView var10000 = (UpdateCarConditionMvpView)this.getView();
      Boolean isRestored = var10000 != null ? var10000.isRestored() : null;
      if (Intrinsics.areEqual(isRestored, true)) {
         this.reassignCarData();
      } else {
         this.setCarData(true);
      }

      try {
         var10000 = (UpdateCarConditionMvpView)this.getView();
         if (var10000 != null) {
            var10000.enableTiresEditing();
         }
      } catch (Throwable var3) {
      }

   }

   private final void checkAxles(CurrentCondition car) {
      Tires var10000 = car.getFrontTires();
      String var2 = var10000 != null ? var10000.getProducer() : null;
      Tires var10001 = car.getBackTires();
      if (Intrinsics.areEqual(var2, var10001 != null ? var10001.getProducer() : null)) {
         var10000 = car.getFrontTires();
         var2 = var10000 != null ? var10000.getSize() : null;
         var10001 = car.getBackTires();
         if (Intrinsics.areEqual(var2, var10001 != null ? var10001.getSize() : null)) {
            var10000 = car.getFrontTires();
            Integer var3 = var10000 != null ? var10000.getType() : null;
            var10001 = car.getBackTires();
            if (Intrinsics.areEqual(var3, var10001 != null ? var10001.getType() : null)) {
               var10000 = car.getFrontTires();
               var3 = var10000 != null ? var10000.getWheelsType() : null;
               var10001 = car.getBackTires();
               if (Intrinsics.areEqual(var3, var10001 != null ? var10001.getWheelsType() : null)) {
                  UpdateCarConditionMvpView var4 = (UpdateCarConditionMvpView)this.getView();
                  if (var4 != null) {
                     var4.hideBackAxle(true);
                  }
               }
            }
         }
      }

   }

   private final void setCarData(boolean refreshRegistration) {
      CarData carData = this.repository.getCarManager().getCarData();
      if (carData != null) {
         CurrentCondition var10000 = carData.getCarCondition();
         if (var10000 != null) {
            CurrentCondition var3 = var10000;
            boolean var4 = false;
            boolean var5 = false;
            int var7 = false;
            this.checkAxles(var3);
            List obligatoryEq = BitUtils.getCarObligatoryEquipment(var3.getObligatoryEquipment());
            List optionalEq = BitUtils.getCarOptionalEquipment(var3.getOptionalEquipment());
            CarManager var13 = this.repository.getCarManager();
            Intrinsics.checkExpressionValueIsNotNull(obligatoryEq, "obligatoryEq");
            var13.setInitialObligatoryEq(obligatoryEq);
            this.repository.getCarManager().setInitialCarCondition(CurrentCondition.copy$default(var3, 0, 0, (Integer)null, (String)null, 0, 0, 0, (Tires)null, (Tires)null, 0, 0, 0, (ArrayList)null, 8191, (Object)null));
            boolean cleanInside = false;
            boolean cleanOutside = false;
            boolean cleanUpholstery = false;
            switch(var3.getPurityState()) {
            case 1:
               cleanInside = true;
               cleanOutside = false;
               cleanUpholstery = false;
               break;
            case 2:
               cleanInside = false;
               cleanOutside = true;
               cleanUpholstery = true;
               break;
            case 3:
               cleanInside = true;
               cleanOutside = true;
               cleanUpholstery = false;
               break;
            case 4:
               cleanInside = false;
               cleanOutside = false;
               cleanUpholstery = true;
               break;
            case 5:
               cleanInside = true;
               cleanOutside = false;
               cleanUpholstery = true;
               break;
            case 6:
               cleanInside = false;
               cleanOutside = true;
               cleanUpholstery = true;
               break;
            case 7:
               cleanInside = true;
               cleanOutside = true;
               cleanUpholstery = true;
               break;
            default:
               cleanInside = false;
               cleanOutside = false;
               cleanUpholstery = false;
            }

            UpdateCarConditionMvpView var14 = (UpdateCarConditionMvpView)this.getView();
            if (var14 != null) {
               var14.setRegistrationNumberInput(refreshRegistration ? carData.getRegistrationNo() : null);
            }

            var14 = (UpdateCarConditionMvpView)this.getView();
            if (var14 != null) {
               var14.setupFirstPart(carData.getRegistrationNo(), carData.getCarBrandName(), carData.getCarModelName(), var3.getMileage(), var3.getFuelState());
            }

            var14 = (UpdateCarConditionMvpView)this.getView();
            if (var14 != null) {
               Tires var10004 = var3.getFrontTires();
               Integer var15 = var10004 != null ? var10004.getType() : null;
               Tires var10005 = var3.getFrontTires();
               String var16 = var10005 != null ? var10005.getProducer() : null;
               Tires var10006 = var3.getFrontTires();
               String var17 = var10006 != null ? var10006.getSize() : null;
               Tires var10007 = var3.getBackTires();
               Integer var18 = var10007 != null ? var10007.getType() : null;
               Tires var10008 = var3.getBackTires();
               String var19 = var10008 != null ? var10008.getProducer() : null;
               Tires var10009 = var3.getBackTires();
               String var20 = var10009 != null ? var10009.getSize() : null;
               Tires var10010 = var3.getFrontTires();
               Integer var21 = var10010 != null ? var10010.getWheelsType() : null;
               Tires var10011 = var3.getBackTires();
               var14.setupSecondPart(cleanInside, cleanOutside, cleanUpholstery, var15, var16, var17, var18, var19, var20, var21, var10011 != null ? var10011.getWheelsType() : null);
            }

            var14 = (UpdateCarConditionMvpView)this.getView();
            if (var14 != null) {
               Intrinsics.checkExpressionValueIsNotNull(optionalEq, "optionalEq");
               var14.setupThirdPart(obligatoryEq, optionalEq, var3.getWheelCupsAmount());
            }

            var14 = (UpdateCarConditionMvpView)this.getView();
            if (var14 != null) {
               var14.observeUpholsteryPurity();
            }
         }
      }

   }

   // $FF: synthetic method
   static void setCarData$default(UpdateCarConditionPresenter var0, boolean var1, int var2, Object var3) {
      if ((var2 & 1) != 0) {
         var1 = true;
      }

      var0.setCarData(var1);
   }

   private final void reassignCarData() {
      CarData car = this.repository.getCarManager().getCarData();
      UpdateCarConditionMvpView var10000 = (UpdateCarConditionMvpView)this.getView();
      if (var10000 != null) {
         var10000.setupFirstPart(car != null ? car.getRegistrationNo() : null, car != null ? car.getCarBrandName() : null, car != null ? car.getCarModelName() : null, (Integer)null, (Integer)null);
      }

   }

   public final void onNextClick() {
      CarData car = this.repository.getCarManager().getCarData();
      if (car == null) {
         RxBus.INSTANCE.post(new ShowMessage(-1900524, (String)null, 2, (DefaultConstructorMarker)null));
      } else {
         this.isFormValid();
      }

   }

   private final void isFormValid() {
      CurrentCondition condition = this.repository.getCarManager().getInitialCarCondition();
      if (condition != null) {
         UpdateCarConditionMvpView var10000 = (UpdateCarConditionMvpView)this.getView();
         Integer course = var10000 != null ? var10000.getCourse() : null;
         if (course != null) {
            if (course < condition.getMileage()) {
               var10000 = (UpdateCarConditionMvpView)this.getView();
               if (var10000 != null) {
                  var10000.showMileageLowerWarning();
               }
            } else {
               int var3 = condition.getMileage();
               if (course == var3) {
                  var10000 = (UpdateCarConditionMvpView)this.getView();
                  if (var10000 != null) {
                     var10000.showMileageNotChangedWarning();
                  }
               } else if (course >= condition.getMileage() + 1000) {
                  var10000 = (UpdateCarConditionMvpView)this.getView();
                  if (var10000 != null) {
                     var10000.showMileageTooHighWarning();
                  }
               } else {
                  this.doNext();
               }
            }
         }
      }

   }

   public final void doNext() {
      this.getInputsAndUpdateCarCondition();
      if (this.repository.getCarManager().getDirtyUpholstery()) {
         Boolean var10000 = this.repository.getCarManager().isDirtyPhotoTaken();
         if (var10000 == null) {
            Intrinsics.throwNpe();
         }

         if (!var10000) {
            UpdateCarConditionMvpView var1 = (UpdateCarConditionMvpView)this.getView();
            if (var1 != null) {
               var1.hideProgress();
            }

            RxBus.INSTANCE.post(new ChangeFragment(40, false, 2, (DefaultConstructorMarker)null));
            this.repository.getCarManager().setDirtyPhotoTaken(false);
            return;
         }
      }

      this.updateCarCondition();
   }

   private final void getInputsAndUpdateCarCondition() {
      CarData initialCar = this.repository.getCarManager().getCarData();
      if (initialCar != null) {
         CurrentCondition var10000 = initialCar.getCarCondition();
         if (var10000 != null) {
            CurrentCondition var2 = var10000;
            boolean var3 = false;
            boolean var4 = false;
            int var6 = false;
            boolean var7 = false;
            boolean var8 = false;
            int var10 = false;
            UpdateCarConditionMvpView var23 = (UpdateCarConditionMvpView)this.getView();
            if (var23 != null) {
               UpdateCarConditionMvpView var11 = var23;
               boolean var12 = false;
               boolean var13 = false;
               int var15 = false;
               int optionalEquipmentSum = 0;
               int obligatoryEquipmentSum = 0;
               int purityStateSum = 0;
               var2.setMileage(var11.getCourse());
               var2.setFuelState(var11.getFuel());
               if (var11.getCleanInside()) {
                  ++purityStateSum;
               }

               if (var11.getCleanOutside()) {
                  purityStateSum += 2;
               }

               if (var11.getCleanUpholstery()) {
                  purityStateSum += 4;
               }

               var2.setPurityState(purityStateSum);
               int tiresType;
               int wheelsType;
               String tiresProducer;
               String tiresSize;
               Tires var24;
               if (var2.getFrontTires() == null) {
                  tiresType = var11.getFrontTiresSummer() ? 0 : (var11.getFrontTiresWinter() ? 1 : 2);
                  wheelsType = var11.getFrontWheelsType() ? 2 : 1;
                  tiresProducer = var11.getFrontTiresManufacturer();
                  tiresSize = var11.getFrontTiresSize();
                  var2.setFrontTires((new Tires()).createTiresObject(tiresType, tiresProducer, tiresSize, wheelsType));
               } else {
                  var24 = var2.getFrontTires();
                  if (var24 != null) {
                     var24.setType(var11.getFrontTiresSummer() ? 0 : (var11.getFrontTiresWinter() ? 1 : 2));
                  }

                  var24 = var2.getFrontTires();
                  if (var24 != null) {
                     var24.setWheelsType(var11.getFrontWheelsType() ? 2 : 1);
                  }

                  var24 = var2.getFrontTires();
                  if (var24 != null) {
                     var24.setProducer(var11.getFrontTiresManufacturer());
                  }

                  var24 = var2.getFrontTires();
                  if (var24 != null) {
                     var24.setSize(var11.getFrontTiresSize());
                  }
               }

               if (var2.getBackTires() == null) {
                  tiresType = var11.getBackTiresSummer() ? 0 : (var11.getBackTiresWinter() ? 1 : 2);
                  wheelsType = var11.getBackWheelsType() ? 2 : 1;
                  tiresProducer = var11.getBackTiresManufacturer();
                  tiresSize = var11.getBackTiresSize();
                  var2.setBackTires((new Tires()).createTiresObject(tiresType, tiresProducer, tiresSize, wheelsType));
               } else {
                  var24 = var2.getBackTires();
                  if (var24 != null) {
                     var24.setType(var11.getBackTiresSummer() ? 0 : (var11.getBackTiresWinter() ? 1 : 2));
                  }

                  var24 = var2.getBackTires();
                  if (var24 != null) {
                     var24.setWheelsType(var11.getBackWheelsType() ? 2 : 1);
                  }

                  var24 = var2.getBackTires();
                  if (var24 != null) {
                     var24.setProducer(var11.getBackTiresManufacturer());
                  }

                  var24 = var2.getBackTires();
                  if (var24 != null) {
                     var24.setSize(var11.getBackTiresSize());
                  }
               }

               var23 = (UpdateCarConditionMvpView)this.getView();
               if (var23 != null) {
                  if (var23.checkSameAxles()) {
                     var24 = var2.getBackTires();
                     Tires var10001;
                     if (var24 != null) {
                        var10001 = var2.getFrontTires();
                        if (var10001 == null) {
                           Intrinsics.throwNpe();
                        }

                        var24.setType(var10001.getType());
                     }

                     var24 = var2.getBackTires();
                     if (var24 != null) {
                        var10001 = var2.getFrontTires();
                        if (var10001 == null) {
                           Intrinsics.throwNpe();
                        }

                        var24.setWheelsType(var10001.getWheelsType());
                     }

                     var24 = var2.getBackTires();
                     if (var24 != null) {
                        var10001 = var2.getFrontTires();
                        if (var10001 == null) {
                           Intrinsics.throwNpe();
                        }

                        var24.setProducer(var10001.getProducer());
                     }

                     var24 = var2.getBackTires();
                     if (var24 != null) {
                        var10001 = var2.getFrontTires();
                        if (var10001 == null) {
                           Intrinsics.throwNpe();
                        }

                        var24.setSize(var10001.getSize());
                     }
                  }
               }

               if (var11.getAntenna()) {
                  optionalEquipmentSum += CarOptionalEquipment.AERIAL;
               }

               if (var11.getBlind()) {
                  optionalEquipmentSum += CarOptionalEquipment.BOOT_COVER;
               }

               if (var11.getSpareWheel()) {
                  optionalEquipmentSum += CarOptionalEquipment.SPARE_WHEEL;
               }

               if (var11.getJack()) {
                  optionalEquipmentSum += CarOptionalEquipment.CAR_LIFTING_TOOLS;
               }

               if (var11.getRepairKit()) {
                  optionalEquipmentSum += CarOptionalEquipment.TIRE_REPAIR_KIT;
               }

               if (var11.getGPS()) {
                  optionalEquipmentSum += CarOptionalEquipment.GPS;
               }

               if (var11.getRoofRailing()) {
                  optionalEquipmentSum += CarOptionalEquipment.ROOF_RAILING;
               }

               if (Intrinsics.areEqual(var11.getSecondKeySet(), true)) {
                  optionalEquipmentSum += CarOptionalEquipment.SECOND_KEY_SET;
               }

               if (Intrinsics.areEqual(var11.getUserManual(), true)) {
                  optionalEquipmentSum += CarOptionalEquipment.USER_MANUAL;
               }

               if (Intrinsics.areEqual(var11.getFloorMats(), true)) {
                  optionalEquipmentSum += CarOptionalEquipment.FLOOR_MATS;
               }

               var2.setWheelCupsAmount(var11.getCaps());
               var2.setOptionalEquipment(optionalEquipmentSum);
               if (var11.getRegistrationDocument()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.REGISTRATION_DOCUMENT;
               }

               if (var11.getOCInsurance()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.OC_INSURANCE;
               }

               if (var11.getFireExtinguisher()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.FIRE_EXTINGUISHER;
               }

               if (var11.getTriangle()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.TRIANGLE;
               }

               if (var11.getFirstAidKit()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.FIRST_AID_KIT;
               }

               if (var11.getKeys()) {
                  obligatoryEquipmentSum += CarObligatoryEquipment.KEYS;
               }

               var2.setObligatoryEquipment(obligatoryEquipmentSum);
            }
         }
      }

   }

   private final void updateCarCondition() {
      CarData var10000 = this.repository.getCarManager().getCarData();
      CurrentCondition carCondition = var10000 != null ? var10000.getCarCondition() : null;
      List userDamages = this.repository.getCarManager().getDamages();
      boolean var4 = false;
      ArrayList allDamages = new ArrayList();
      ArrayList var7;
      if (carCondition != null) {
         var7 = carCondition.getDamages();
         if (var7 != null) {
            var7 = (ArrayList)CollectionsKt.toCollection((Iterable)var7, (Collection)allDamages);
         }
      }

      CurrentCondition updatedCondition = carCondition != null ? CurrentCondition.copy$default(carCondition, 0, 0, (Integer)null, (String)null, 0, 0, 0, (Tires)null, (Tires)null, 0, 0, 0, allDamages, 4095, (Object)null) : null;
      if (updatedCondition != null) {
         var7 = updatedCondition.getDamages();
         if (var7 != null) {
            var7.addAll((Collection)userDamages);
         }
      }

      Task task = this.repository.getCarManager().getCurrentTask();
      if (task != null) {
         if (!this.repository.getCarManager().isPickCar() && !this.repository.getCarManager().isHandCar()) {
            this.composite.add(this.repository.updateCarCondition(updatedCondition).subscribe((Consumer)(new Consumer() {
               // $FF: synthetic method
               // $FF: bridge method
               public void accept(Object var1) {
                  this.accept((CurrentCondition)var1);
               }

               public final void accept(CurrentCondition it) {
                  UpdateCarConditionPresenter.this.repository.getCarManager().getPhotos().clear();
                  UpdateCarConditionPresenter.this.repository.getCarManager().setInitialCarCondition(it);
                  UpdateCarConditionMvpView var10000 = UpdateCarConditionPresenter.access$getView(UpdateCarConditionPresenter.this);
                  if (var10000 != null) {
                     var10000.hideProgress();
                  }

                  RxBus.INSTANCE.post(new ChangeFragment(8, false, 2, (DefaultConstructorMarker)null));
               }
            }), (Consumer)(new Consumer() {
               // $FF: synthetic method
               // $FF: bridge method
               public void accept(Object var1) {
                  this.accept((Throwable)var1);
               }

               public final void accept(Throwable t) {
                  UpdateCarConditionMvpView var10000 = UpdateCarConditionPresenter.access$getView(UpdateCarConditionPresenter.this);
                  if (var10000 != null) {
                     var10000.hideProgress();
                  }

                  RxBus var2 = RxBus.INSTANCE;
                  Intrinsics.checkExpressionValueIsNotNull(t, "t");
                  var2.post(new ErrorMessage(t, -1900182));
               }
            })));
         }
      } else {
         this.composite.add(this.repository.updateCarCondition(updatedCondition).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
               this.accept((CurrentCondition)var1);
            }

            public final void accept(CurrentCondition it) {
               UpdateCarConditionPresenter.this.repository.getCarManager().getDamages().clear();
               UpdateCarConditionPresenter.this.repository.getCarManager().getPhotos().clear();
               UpdateCarConditionMvpView var10000 = UpdateCarConditionPresenter.access$getView(UpdateCarConditionPresenter.this);
               if (var10000 != null) {
                  var10000.hideProgress();
               }

               RxBus.INSTANCE.post(new ShowMain());
            }
         }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
               this.accept((Throwable)var1);
            }

            public final void accept(Throwable t) {
               UpdateCarConditionMvpView var10000 = UpdateCarConditionPresenter.access$getView(UpdateCarConditionPresenter.this);
               if (var10000 != null) {
                  var10000.hideProgress();
               }

               RxBus var2 = RxBus.INSTANCE;
               Intrinsics.checkExpressionValueIsNotNull(t, "t");
               var2.post(new ErrorMessage(t, 0, 2, (DefaultConstructorMarker)null));
            }
         })));
      }

   }

   public final void markUpholsteryPurity(boolean dirty) {
      this.repository.getCarManager().setDirtyUpholstery(dirty);
   }

   public final boolean shouldClearTires() {
      boolean var1;
      label34: {
         TaskDetails var10000;
         if (this.repository.getCarManager().isStarting()) {
            var10000 = this.repository.getCarManager().getCurrentTaskDetails();
            if (var10000 != null) {
               if (var10000.getType() == Type.INSTANCE.getTRANSPORT_CAR_FROM_DEALER()) {
                  break label34;
               }
            }
         }

         if (this.repository.getCarManager().isFinishing()) {
            var10000 = this.repository.getCarManager().getCurrentTaskDetails();
            if (var10000 != null) {
               if (var10000.getType() == Type.INSTANCE.getTRANSPORT_CAR_TO_DEALER()) {
                  break label34;
               }
            }
         }

         var1 = false;
         return var1;
      }

      var1 = true;
      return var1;
   }

   protected void onAttachView() {
   }

   protected void onDetachView() {
   }

   @Inject
   public UpdateCarConditionPresenter(@NotNull Repository repository) {
      Intrinsics.checkParameterIsNotNull(repository, "repository");
      super();
      this.repository = repository;
      this.composite = new CompositeDisposable();
   }

   // $FF: synthetic method
   public static final UpdateCarConditionMvpView access$getView(UpdateCarConditionPresenter $this) {
      return (UpdateCarConditionMvpView)$this.getView();
   }
}
