package com.corcode.asystententerprise.UI.Reservations;

import com.corcode.asystententerprise.Base.BasePresenter;
import com.corcode.asystententerprise.Bus.ErrorMessage;
import com.corcode.asystententerprise.Bus.PerformDepartmentSearch;
import com.corcode.asystententerprise.Bus.RxBus;
import com.corcode.asystententerprise.Bus.ShowMessage;
import com.corcode.asystententerprise.Persistance.Repository;
import com.corcode.asystententerprise.Schemas.GetDepartmentsParams;
import com.corcode.asystententerprise.Schemas.GetReservationParams;
import com.corcode.asystententerprise.Schemas.Reservation;
import com.corcode.asystententerprise.Schemas.ResponseGetDepartments;
import com.corcode.asystententerprise.Schemas.ResponseGetReservation;
import com.corcode.asystententerprise.Utils.Utils;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.joda.time.DateTime;

@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\n\u001a\u00020\u000bH\u0002J\b\u0010\f\u001a\u00020\u000bH\u0002J\b\u0010\r\u001a\u00020\u000bH\u0014J\b\u0010\u000e\u001a\u00020\u000bH\u0014J\u0006\u0010\u000f\u001a\u00020\u000bJ\u0006\u0010\u0010\u001a\u00020\u000bJ\u0010\u0010\u0011\u001a\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013J\u000e\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\tJ\u0006\u0010\u0016\u001a\u00020\u000bJ\u0006\u0010\u0017\u001a\u00020\u000bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"},
   d2 = {"Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;", "Lcom/corcode/asystententerprise/Base/BasePresenter;", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsView;", "repository", "Lcom/corcode/asystententerprise/Persistance/Repository;", "(Lcom/corcode/asystententerprise/Persistance/Repository;)V", "composite", "Lio/reactivex/disposables/CompositeDisposable;", "dataLoaded", "", "getDepartments", "", "getSortedReservations", "onAttachView", "onDetachView", "onSearchClick", "registerForDepartmentSearchRequest", "setFlagForNote", "reservation", "Lcom/corcode/asystententerprise/Schemas/Reservation;", "setupEditingButton", "enabled", "start", "stop", "app_debug"}
)
public final class ReservationsPresenter extends BasePresenter {
   private final CompositeDisposable composite;
   private boolean dataLoaded;
   private final Repository repository;

   public final void start() {
      if (!this.dataLoaded) {
         GetReservationParams params = new GetReservationParams((Integer)null, (Integer)null, (Integer)null, (String)null, (String)null, (Integer)null, 63, (DefaultConstructorMarker)null);
         params.setOrder(2);
         params.setState(1);
         if (this.repository.getCarManager().getSearchingDepartmentId() != null) {
            params.setHireDepartmentId(this.repository.getCarManager().getSearchingDepartmentId());
         } else {
            params.setHireDepartmentId(this.repository.getUserDepartmentId());
         }

         params.setStartDateFrom(Utils.INSTANCE.getDateAsString(DateTime.now().withTimeAtStartOfDay().toDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS"));
         params.setStartDateTo(Utils.INSTANCE.getDateAsString(DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS"));
         ReservationsView var10000 = (ReservationsView)this.getView();
         if (var10000 != null) {
            var10000.showProgress();
         }

         this.composite.add(this.repository.getReservation(params).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
               this.accept((ResponseGetReservation)var1);
            }

            public final void accept(ResponseGetReservation response) {
               ReservationsPresenter.this.dataLoaded = true;
               ReservationsView var10000;
               if (response.getReservationList().isEmpty()) {
                  ReservationsPresenter.this.repository.getCarManager().setDataLoaded(false);
                  var10000 = ReservationsPresenter.access$getView(ReservationsPresenter.this);
                  if (var10000 != null) {
                     var10000.hideProgress();
                  }
               } else {
                  boolean var3 = false;
                  List reservationsArray = (List)(new ArrayList());
                  Iterable $this$map$iv = (Iterable)response.getReservationList();
                  int $i$f$map = false;
                  Collection destination$iv$iv = (Collection)(new ArrayList(CollectionsKt.collectionSizeOrDefault($this$map$iv, 10)));
                  int $i$f$mapTo = false;
                  Iterator var8 = $this$map$iv.iterator();

                  while(var8.hasNext()) {
                     Object item$iv$iv = var8.next();
                     Reservation it = (Reservation)item$iv$iv;
                     int var11 = false;
                     Boolean var13 = reservationsArray.add(it);
                     destination$iv$iv.add(var13);
                  }

                  List var15 = (List)destination$iv$iv;
                  var10000 = ReservationsPresenter.access$getView(ReservationsPresenter.this);
                  if (var10000 != null) {
                     var10000.updateAdapter(reservationsArray);
                  }

                  var10000 = ReservationsPresenter.access$getView(ReservationsPresenter.this);
                  if (var10000 != null) {
                     var10000.hideProgress();
                  }
               }

            }
         }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
               this.accept((Throwable)var1);
            }

            public final void accept(Throwable t) {
               ReservationsView var10000 = ReservationsPresenter.access$getView(ReservationsPresenter.this);
               if (var10000 != null) {
                  var10000.hideProgress();
               }

               RxBus var2 = RxBus.INSTANCE;
               Intrinsics.checkExpressionValueIsNotNull(t, "t");
               var2.post(new ErrorMessage(t, 0, 2, (DefaultConstructorMarker)null));
            }
         })));
         this.getDepartments();
      }

   }

   private final void getDepartments() {
      this.composite.add(this.repository.getDepartments(new GetDepartmentsParams(0, 1, (DefaultConstructorMarker)null)).subscribe((Consumer)(new Consumer() {
         // $FF: synthetic method
         // $FF: bridge method
         public void accept(Object var1) {
            this.accept((ResponseGetDepartments)var1);
         }

         public final void accept(ResponseGetDepartments it) {
            ReservationsPresenter.this.repository.getFleetManager().getDepartments().addAll((Collection)it.getDepartmentsList());
         }
      })));
   }

   public final void registerForDepartmentSearchRequest() {
      this.composite.add(RxBus.INSTANCE.registerMainThread(Reflection.getOrCreateKotlinClass(PerformDepartmentSearch.class)).subscribe((Consumer)(new Consumer() {
         // $FF: synthetic method
         // $FF: bridge method
         public void accept(Object var1) {
            this.accept((PerformDepartmentSearch)var1);
         }

         public final void accept(PerformDepartmentSearch it) {
            ReservationsPresenter.this.getSortedReservations();
         }
      })));
   }

   private final void getSortedReservations() {
      this.dataLoaded = false;
      ReservationsView var10000 = (ReservationsView)this.getView();
      if (var10000 != null) {
         var10000.clearData();
      }

      this.start();
   }

   public final void setupEditingButton(boolean enabled) {
      ReservationsView var10000 = (ReservationsView)this.getView();
      if (var10000 != null) {
         var10000.setupEditingButton(enabled);
      }

   }

   public final void setFlagForNote(@Nullable Reservation reservation) {
      this.repository.getCarManager().setSelectedReservation(reservation);
      this.repository.getCarManager().setNoteForReservation(true);
   }

   public final void onSearchClick() {
      Collection var1 = (Collection)this.repository.getFleetManager().getDepartments();
      boolean var2 = false;
      if (!var1.isEmpty()) {
         ReservationsView var10000 = (ReservationsView)this.getView();
         if (var10000 != null) {
            var10000.showSearchDialog();
         }
      } else {
         this.getDepartments();
         RxBus.INSTANCE.post(new ShowMessage(-1900820, (String)null, 2, (DefaultConstructorMarker)null));
      }

   }

   protected void onAttachView() {
   }

   protected void onDetachView() {
   }

   public final void stop() {
      this.composite.clear();
   }

   @Inject
   public ReservationsPresenter(@NotNull Repository repository) {
      super();
      Intrinsics.checkParameterIsNotNull(repository, "repository");
      this.repository = repository;
      this.composite = new CompositeDisposable();
   }

   // $FF: synthetic method
   public static final boolean access$getDataLoaded$p(ReservationsPresenter $this) {
      return $this.dataLoaded;
   }

   // $FF: synthetic method
   public static final ReservationsView access$getView(ReservationsPresenter $this) {
      return (ReservationsView)$this.getView();
   }
}
