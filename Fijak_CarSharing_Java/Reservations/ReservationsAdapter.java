// ReservationsAdapter.java
package com.corcode.asystententerprise.UI.Reservations;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.corcode.asystententerprise.R.id;
import com.corcode.asystententerprise.Schemas.Reservation;
import com.corcode.asystententerprise.Utils.Extensions;
import com.corcode.asystententerprise.Utils.Utils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001cB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0006\u0010\u000e\u001a\u00020\u000fJ\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u001c\u0010\u0012\u001a\u00020\u000f2\n\u0010\u0013\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0011H\u0016J\u001c\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0011H\u0016J\u0014\u0010\u0019\u001a\u00020\u000f2\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\b0\u001bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r¨\u0006\u001d"},
   d2 = {"Lcom/corcode/asystententerprise/UI/Reservations/ReservationsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsAdapter$ReservationHolder;", "presenter", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;", "(Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;)V", "reservations", "", "Lcom/corcode/asystententerprise/Schemas/Reservation;", "selectedReservation", "getSelectedReservation", "()Lcom/corcode/asystententerprise/Schemas/Reservation;", "setSelectedReservation", "(Lcom/corcode/asystententerprise/Schemas/Reservation;)V", "clearData", "", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "updateData", "data", "", "ReservationHolder", "app_debug"}
)
public final class ReservationsAdapter extends Adapter {
   @Nullable
   private Reservation selectedReservation;
   private final List reservations;
   private final ReservationsPresenter presenter;

   @Nullable
   public final Reservation getSelectedReservation() {
      return this.selectedReservation;
   }

   public final void setSelectedReservation(@Nullable Reservation var1) {
      this.selectedReservation = var1;
   }

   public final void updateData(@NotNull List data) {
      Intrinsics.checkParameterIsNotNull(data, "data");
      this.reservations.clear();
      this.reservations.addAll((Collection)data);
      this.notifyDataSetChanged();
   }

   public final void clearData() {
      this.reservations.clear();
      this.notifyDataSetChanged();
   }

   @NotNull
   public ReservationsAdapter.ReservationHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
      Intrinsics.checkParameterIsNotNull(parent, "parent");
      return new ReservationsAdapter.ReservationHolder(Extensions.inflate$default(parent, -1300132, false, 2, (Object)null));
   }

   // $FF: synthetic method
   // $FF: bridge method
   public ViewHolder onCreateViewHolder(ViewGroup var1, int var2) {
      return (ViewHolder)this.onCreateViewHolder(var1, var2);
   }

   public void onBindViewHolder(@NotNull ReservationsAdapter.ReservationHolder holder, int position) {
      Intrinsics.checkParameterIsNotNull(holder, "holder");
      holder.setupReservation((Reservation)this.reservations.get(position));
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void onBindViewHolder(ViewHolder var1, int var2) {
      this.onBindViewHolder((ReservationsAdapter.ReservationHolder)var1, var2);
   }

   public int getItemCount() {
      return this.reservations.size();
   }

   public ReservationsAdapter(@NotNull ReservationsPresenter presenter) {
      super();
      Intrinsics.checkParameterIsNotNull(presenter, "presenter");
      this.presenter = presenter;
      boolean var2 = false;
      List var4 = (List)(new ArrayList());
      this.reservations = var4;
   }

   // $FF: synthetic method
   public static final List access$getReservations$p(ReservationsAdapter $this) {
      return $this.reservations;
   }

   @Metadata(
      mv = {1, 1, 15},
      bv = {1, 0, 3},
      k = 1,
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lcom/corcode/asystententerprise/UI/Reservations/ReservationsAdapter$ReservationHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lcom/corcode/asystententerprise/UI/Reservations/ReservationsAdapter;Landroid/view/View;)V", "isSelected", "", "setupReservation", "", "reservation", "Lcom/corcode/asystententerprise/Schemas/Reservation;", "app_debug"}
   )
   public final class ReservationHolder extends ViewHolder {
      private boolean isSelected;

      public final void setupReservation(@NotNull Reservation reservation) {
         Intrinsics.checkParameterIsNotNull(reservation, "reservation");
         View var2 = this.itemView;
         boolean var3 = false;
         boolean var4 = false;
         View $this$with = var2;
         int var6 = false;
         String var7 = reservation.getNumber();
         boolean textColor = false;
         boolean var9 = false;
         int var11 = false;
         TextView var10000 = (TextView)var2.findViewById(id.item_reservation_order_number);
         if (var10000 != null) {
            var10000.setText((CharSequence)var7);
         }

         Date var15;
         try {
            var15 = reservation.getHireDateFrom();
            textColor = false;
            var9 = false;
            var11 = false;
            var10000 = (TextView)$this$with.findViewById(id.item_reservation_start_date);
            if (var10000 != null) {
               var10000.setText((CharSequence)Utils.INSTANCE.getDateAsString(var15, "yyy-MM-dd HH:mm"));
            }
         } catch (Throwable var14) {
         }

         try {
            var15 = reservation.getReturnDateTo();
            textColor = false;
            var9 = false;
            var11 = false;
            var10000 = (TextView)$this$with.findViewById(id.item_reservation_end_date);
            if (var10000 != null) {
               var10000.setText((CharSequence)Utils.INSTANCE.getDateAsString(var15, "yyy-MM-dd HH:mm"));
            }
         } catch (Throwable var13) {
         }

         try {
            String[] var16 = reservation.getCarRegistrationNumber();
            textColor = false;
            var9 = false;
            var11 = false;
            var10000 = (TextView)$this$with.findViewById(id.item_reservation_car_registration_number);
            if (var10000 != null) {
               var10000.setText((CharSequence)(var16 != null ? var16[0] : null));
            }
         } catch (Throwable var12) {
         }

         var7 = reservation.getClientName();
         textColor = false;
         var9 = false;
         var11 = false;
         var10000 = (TextView)var2.findViewById(id.item_reservation_client);
         if (var10000 != null) {
            var10000.setText((CharSequence)var7);
         }

         var7 = reservation.getOrderedAcriss();
         textColor = false;
         var9 = false;
         var11 = false;
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_ordered_segment);
         if (var10000 != null) {
            var10000.setText((CharSequence)var7);
         }

         var7 = reservation.getLetOutAcriss();
         textColor = false;
         var9 = false;
         var11 = false;
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_let_out_segment);
         if (var10000 != null) {
            var10000.setText((CharSequence)var7);
         }

         this.isSelected = reservation.getSelected();
         int backgroundColor = false;
         textColor = false;
         int backgroundColorx;
         int textColorx;
         if (this.isSelected) {
            backgroundColorx = -500153;
            textColorx = -500093;
         } else {
            backgroundColorx = -500093;
            textColorx = -500051;
         }

         var10000 = (TextView)var2.findViewById(id.item_reservation_order_number);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_order_number");
         Context var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_start_date);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_start_date");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_end_date);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_end_date");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_registration_number);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_car_registration_number");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_client);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_client");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_ordered_segment);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_car_ordered_segment");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_let_out_segment);
         Intrinsics.checkExpressionValueIsNotNull(var10000, "item_reservation_car_let_out_segment");
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setBackground(ContextCompat.getDrawable(var10001, backgroundColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_order_number);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_start_date);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_end_date);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_registration_number);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_client);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_ordered_segment);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var10000 = (TextView)var2.findViewById(id.item_reservation_car_let_out_segment);
         var10001 = var2.getContext();
         if (var10001 == null) {
            Intrinsics.throwNpe();
         }

         var10000.setTextColor(ContextCompat.getColor(var10001, textColorx));
         var2.setOnClickListener((OnClickListener)(new ReservationsAdapter$ReservationHolder$setupReservation$$inlined$with$lambda$1(this, reservation)));
         if (ReservationsAdapter.this.getSelectedReservation() != null) {
            ReservationsAdapter.this.presenter.setupEditingButton(true);
         } else {
            ReservationsAdapter.this.presenter.setupEditingButton(false);
         }

      }

      public ReservationHolder(@NotNull View view) {
         Intrinsics.checkParameterIsNotNull(view, "view");
         super(view);
      }
   }
}


@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 3,
   d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005¨\u0006\u0006"},
   d2 = {"<anonymous>", "", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick", "com/corcode/asystententerprise/UI/Reservations/ReservationsAdapter$ReservationHolder$setupReservation$1$8"}
)
final class ReservationsAdapter$ReservationHolder$setupReservation$$inlined$with$lambda$1 implements OnClickListener {
   // $FF: synthetic field
   final ReservationsAdapter.ReservationHolder this$0;
   // $FF: synthetic field
   final Reservation $reservation$inlined;

   ReservationsAdapter$ReservationHolder$setupReservation$$inlined$with$lambda$1(ReservationsAdapter.ReservationHolder var1, Reservation var2) {
      this.this$0 = var1;
      this.$reservation$inlined = var2;
   }

   public final void onClick(View it) {
      if (this.$reservation$inlined.getSelected()) {
         this.$reservation$inlined.setSelected(false);
         this.this$0.this$0.setSelectedReservation((Reservation)null);
         this.this$0.this$0.notifyDataSetChanged();
      } else {
         Iterable $this$map$iv = (Iterable)ReservationsAdapter.access$getReservations$p(this.this$0.this$0);
         int $i$f$map = false;
         Collection destination$iv$iv = (Collection)(new ArrayList(CollectionsKt.collectionSizeOrDefault($this$map$iv, 10)));
         int $i$f$mapTo = false;
         Iterator var7 = $this$map$iv.iterator();

         while(var7.hasNext()) {
            Object item$iv$iv = var7.next();
            Reservation it = (Reservation)item$iv$iv;
            int var11 = false;
            it.setSelected(false);
            Unit var12 = Unit.INSTANCE;
            destination$iv$iv.add(var12);
         }

         List var10000 = (List)destination$iv$iv;
         this.this$0.this$0.setSelectedReservation(this.$reservation$inlined);
         this.$reservation$inlined.setSelected(true);
         this.this$0.this$0.notifyDataSetChanged();
      }

   }
}
