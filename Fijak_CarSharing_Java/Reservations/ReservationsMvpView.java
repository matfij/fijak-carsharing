package com.corcode.asystententerprise.UI.Reservations;

import com.corcode.asystententerprise.Base.MvpView;
import java.util.List;
import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;

public interface ReservationsView extends MvpView {
   void setupEditingButton(boolean var1);

   void updateAdapter(@NotNull List var1);

   void showNoReservationError();

   void showProgress();

   void hideProgress();

   void showSearchDialog();

   void clearData();
}
