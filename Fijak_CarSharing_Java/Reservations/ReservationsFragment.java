package com.corcode.asystententerprise.UI.Reservations;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import com.corcode.asystententerprise.App;
import com.corcode.asystententerprise.Base.MvpView;
import com.corcode.asystententerprise.Bus.ChangeFragment;
import com.corcode.asystententerprise.Bus.ProgressVisibility;
import com.corcode.asystententerprise.Bus.RxBus;
import com.corcode.asystententerprise.R.id;
import com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView;
import com.corcode.asystententerprise.UI.Reservations.departmentsearch.DepartmentSearchDialog;
import com.corcode.asystententerprise.Utils.Extensions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 1, 15},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u000fH\u0016J\u001c\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J&\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0014\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0012\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u001a\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020\u00172\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010#\u001a\u00020\u000fH\u0002J\u0010\u0010$\u001a\u00020\u000f2\u0006\u0010%\u001a\u00020\u001eH\u0016J\b\u0010&\u001a\u00020\u000fH\u0002J\b\u0010'\u001a\u00020\u000fH\u0016J\b\u0010(\u001a\u00020\u000fH\u0016J\b\u0010)\u001a\u00020\u000fH\u0016J\u0016\u0010*\u001a\u00020\u000f2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082.¢\u0006\u0002\n\u0000¨\u0006."},
   d2 = {"Lcom/corcode/asystententerprise/UI/Reservations/ReservationsFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsView;", "()V", "adapter", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsAdapter;", "presenter", "Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;", "getPresenter", "()Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;", "setPresenter", "(Lcom/corcode/asystententerprise/UI/Reservations/ReservationsPresenter;)V", "searchDialog", "Lcom/corcode/asystententerprise/UI/Reservations/departmentsearch/DepartmentSearchDialog;", "clearData", "", "hideProgress", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onViewCreated", "view", "setOnClickListeners", "setupEditingButton", "enabled", "setupRecycler", "showNoReservationError", "showProgress", "showSearchDialog", "updateAdapter", "orders", "", "Lcom/corcode/asystententerprise/Schemas/Reservation;", "app_debug"}
)
public final class ReservationsFragment extends Fragment implements ReservationsView {
   @Inject
   @NotNull
   public ReservationsPresenter presenter;
   private ReservationsAdapter adapter;
   private DepartmentSearchDialog searchDialog;
   private HashMap _$_findViewCache;

   @NotNull
   public final ReservationsPresenter getPresenter() {
      ReservationsPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      return var10000;
   }

   public final void setPresenter(@NotNull ReservationsPresenter var1) {
      Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
      this.presenter = var1;
   }

   @Nullable
   public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      Intrinsics.checkParameterIsNotNull(inflater, "inflater");
      View view = inflater.inflate(-1300131, container, false);
      App.Companion.getComponent().inject(this);
      ReservationsPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.attachView((MvpView)this);
      FragmentActivity var5 = this.getActivity();
      if (var5 == null) {
         throw new TypeCastException("null cannot be cast to non-null type com.corcode.asystententerprise.UI.NavigationActivity.NavigationMvpView");
      } else {
         ((NavigationMvpView)var5).setToolbarTitle(-1900585);
         this.setHasOptionsMenu(true);
         return view;
      }
   }

   public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
      Intrinsics.checkParameterIsNotNull(view, "view");
      this.setupEditingButton(false);
      ReservationsPresenter var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.start();
      var10000 = this.presenter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      var10000.registerForDepartmentSearchRequest();
      this.setupRecycler();
      this.setOnClickListeners();
   }

   public void onCreateOptionsMenu(@Nullable Menu menu, @Nullable MenuInflater inflater) {
      if (inflater != null) {
         inflater.inflate(-1400001, menu);
      }

   }

   public boolean onOptionsItemSelected(@Nullable MenuItem item) {
      boolean var2;
      if (item != null) {
         if (item.getItemId() == -1000884) {
            ReservationsPresenter var10000 = this.presenter;
            if (var10000 == null) {
               Intrinsics.throwUninitializedPropertyAccessException("presenter");
            }

            var10000.onSearchClick();
            var2 = true;
            return var2;
         }
      }

      var2 = super.onOptionsItemSelected(item);
      return var2;
   }

   public void showSearchDialog() {
      this.searchDialog = new DepartmentSearchDialog();
      DepartmentSearchDialog var10000 = this.searchDialog;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("searchDialog");
      }

      var10000.show(this.getChildFragmentManager(), DepartmentSearchDialog.Companion.getTAG());
   }

   private final void setOnClickListeners() {
      FloatingActionButton var10000 = (FloatingActionButton)this._$_findCachedViewById(id.reservations_edit_description_button);
      if (var10000 != null) {
         var10000.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View it) {
               ReservationsFragment.this.getPresenter().setFlagForNote(ReservationsFragment.access$getAdapter$p(ReservationsFragment.this).getSelectedReservation());
               RxBus.INSTANCE.post(new ChangeFragment(8, false, 2, (DefaultConstructorMarker)null));
            }
         }));
      }

   }

   public void setupEditingButton(boolean enabled) {
      FloatingActionButton var10000;
      if (enabled) {
         var10000 = (FloatingActionButton)this._$_findCachedViewById(id.reservations_edit_description_button);
         if (var10000 != null) {
            var10000.setEnabled(true);
         }

         var10000 = (FloatingActionButton)this._$_findCachedViewById(id.reservations_edit_description_button);
         if (var10000 != null) {
            var10000.setAlpha(1.0F);
         }
      } else {
         var10000 = (FloatingActionButton)this._$_findCachedViewById(id.reservations_edit_description_button);
         if (var10000 != null) {
            var10000.setEnabled(false);
         }

         var10000 = (FloatingActionButton)this._$_findCachedViewById(id.reservations_edit_description_button);
         if (var10000 != null) {
            var10000.setAlpha(0.5F);
         }
      }

   }

   private final void setupRecycler() {
      ReservationsAdapter var10001 = new ReservationsAdapter();
      ReservationsPresenter var10003 = this.presenter;
      if (var10003 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("presenter");
      }

      this.adapter = var10001;
      RecyclerView var10000 = (RecyclerView)this._$_findCachedViewById(id.reservations_recycler);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "reservations_recycler");
      LinearLayoutManager var1 = new LinearLayoutManager();
      Context var2 = this.getContext();
      if (var2 == null) {
         Intrinsics.throwNpe();
      }

      var10000.setLayoutManager((LayoutManager)var1);
      var10000 = (RecyclerView)this._$_findCachedViewById(id.reservations_recycler);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "reservations_recycler");
      var10001 = this.adapter;
      if (var10001 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("adapter");
      }

      var10000.setAdapter((Adapter)var10001);
   }

   public void updateAdapter(@NotNull List orders) {
      Intrinsics.checkParameterIsNotNull(orders, "orders");
      ReservationsAdapter var10000 = this.adapter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("adapter");
      }

      var10000.updateData(orders);
   }

   public void showNoReservationError() {
      Builder var10000 = new Builder();
      Context var10002 = this.getContext();
      if (var10002 == null) {
         Intrinsics.throwNpe();
      }

      Builder builder = var10000;
      builder.setTitle((CharSequence)this.getString(-1900655));
      builder.setMessage((CharSequence)this.getString(-1900681));
      builder.setNeutralButton(-1900036, (android.content.DialogInterface.OnClickListener)null.INSTANCE);
      builder.show();
   }

   public void clearData() {
      ReservationsAdapter var10000 = this.adapter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("adapter");
      }

      var10000.clearData();
   }

   public void showProgress() {
      RecyclerView var10000 = (RecyclerView)this._$_findCachedViewById(id.reservations_recycler);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "reservations_recycler");
      Extensions.invisible((View)var10000);
      RxBus.INSTANCE.post(new ProgressVisibility(true));
   }

   public void hideProgress() {
      RxBus.INSTANCE.post(new ProgressVisibility(false));
      RecyclerView var10000 = (RecyclerView)this._$_findCachedViewById(id.reservations_recycler);
      Intrinsics.checkExpressionValueIsNotNull(var10000, "reservations_recycler");
      Extensions.visible((View)var10000);
   }

   // $FF: synthetic method
   public static final ReservationsAdapter access$getAdapter$p(ReservationsFragment $this) {
      ReservationsAdapter var10000 = $this.adapter;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("adapter");
      }

      return var10000;
   }

   // $FF: synthetic method
   public static final void access$setAdapter$p(ReservationsFragment $this, ReservationsAdapter var1) {
      $this.adapter = var1;
   }

   public View _$_findCachedViewById(int var1) {
      if (this._$_findViewCache == null) {
         this._$_findViewCache = new HashMap();
      }

      View var2 = (View)this._$_findViewCache.get(var1);
      if (var2 == null) {
         View var10000 = this.getView();
         if (var10000 == null) {
            return null;
         }

         var2 = var10000.findViewById(var1);
         this._$_findViewCache.put(var1, var2);
      }

      return var2;
   }

   public void _$_clearFindViewByIdCache() {
      if (this._$_findViewCache != null) {
         this._$_findViewCache.clear();
      }

   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this._$_clearFindViewByIdCache();
   }
}
